classdef BaselineLearningAlgorithm < LearningAlgorithm
    % BaselineLearningAlgorithm - Baseline model for comparing performances
    %   A BaselineLearningAlgorithm is a model with no parameters that can be 
    %   used to compare performances with respect to the simplest possible baseline.
    %
    %   - For regression, BaselineLearningAlgorithm always output the mean value of the
    %   output elements in the training set.
    %   - For classification, BaselineLearningAlgorithm outputs random labels, in the same
    %   proportion with respect to the training set.

    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        avValue;        % Mean value for regression
        distribution;   % Output distribution for classification
        values;         % Classes
    end
    
    methods
        
        function obj = BaselineLearningAlgorithm(name)
            obj = obj@LearningAlgorithm(name);
        end
        
        function obj = init(obj, d)
        end
        
        function [obj, trainInfo] = train(obj, d)
            
            tic;
            
            % Get training data
            Ytr = d.Y;
            
            % Initialize empty output structure
            trainInfo = struct();
            
            if(d.task == Tasks.R)
                obj.avValue  = mean(Ytr);
            else
                [obj.distribution, obj.values] = hist(Ytr, unique(Ytr));
                obj.distribution = obj.distribution./norm(obj.distribution,1);
            end
            
            trainInfo.training_time = toc;
            
        end
        
        function [labels, scores] = test(obj, d)

            % Get training data
            Xts = d.X;
            
            if(d.task == Tasks.R)
                % In case of regression, always return the mean value of
                % the training set
                labels = obj.avValue*ones(size(Xts, 1), 1);
                scores = labels;
            else
                % In case of classification, sample with respect to the
                % distribution of the training data
                samp = discretesample(obj.distribution, size(Xts, 1))';
                labels = (obj.values(samp))';
                if(size(labels, 1) == 1)
                    labels = labels';
                end
                scores = labels;
            end
            
        end
        
    end
    
end

