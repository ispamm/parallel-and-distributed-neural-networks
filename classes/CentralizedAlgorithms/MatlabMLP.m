classdef MatlabMLP < MultilayerPerceptron
    % MatlabMLP - MLP trained using the Neural Network's toolbox of MATLAB.
    %   You can set the training function with the 'trainAlgo' property.
    %   To see all available training functions (depending on your MATLAB's
    %   version, use the following command:
    %       web(fullfile(docroot,
    %       'nnet/ug/train-and-apply-multilayer-neural-networks.html'))
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        trainAlgo;	% Training algorithm (string)
        lambda;     % Regularization coefficient              
        epochs;     % Number of epochs
        threshold;  % Threshold on the gradient's norm
        savedNet;   % Internal MATLAB network
    end
    
    methods
        function obj = MatlabMLP(name, N_layers, varargin)
            obj = obj@MultilayerPerceptron(name, N_layers);
            p = inputParser;
            p.addParamValue('trainAlgo', 'trainlm');
            p.addParamValue('epochs', 250);
            p.addParamValue('lambda', 0.2);
            p.addParamValue('threshold', 10^-6);
            p.parse(varargin{:});
            obj.trainAlgo = p.Results.trainAlgo;
            obj.epochs = p.Results.epochs;
            obj.lambda = p.Results.lambda;
            obj.threshold = p.Results.threshold;
        end
        
        function [obj, trainInfo] = train(obj, dataset)
            
            obj = obj.startTimer(true);
            
            net = obj.initMATLABnet(dataset);
            
            % Train the network
            [net, tr] = train(net, dataset.X', dataset.Y');
            
            obj.savedNet = net;
            
            % Extract the trained weights
            obj.W_in_h(1:end-1, :) = net.IW{1}';
            obj.W_h_out(1:end-1, :) = net.LW{2}';
            obj.W_in_h(end, :) = net.b{1}';
            obj.W_h_out(end, :) = net.b{2}';
            
            % Save the evaluation of cost function and gradient norm
            % (padding with zeros on the right)
            trainInfo.fval = [tr.perf ones(1, obj.epochs - length(tr.perf) + 1)*tr.perf(end)]';
            trainInfo.gval = [tr.gradient ones(1, obj.epochs - length(tr.gradient) + 1)*tr.gradient(end)]';
            
            % Save the training time
            obj = obj.stopTimer();
            trainInfo.training_time = obj.training_time;
            
        end
        
        function net = initMATLABnet(obj, dataset)
            % Create the network object
            net = feedforwardnet(obj.N_hidden);

            % Set custom parameters
            net.trainFcn = obj.trainAlgo;                   % Set training algorithm
            net.trainParam.showWindow = false;              % Disable output
            net.trainParam.epochs = obj.epochs;             % Set maximum number of iterations
            net.trainParam.min_grad = obj.threshold;        % Set minimum gradient
            if(strcmp(net.trainFcn, 'trainlm'))
                net.trainParam.mu_dec = 0.9;                    % Mu decrement (for trainlm)
                net.trainParam.mu_inc = 1.1;                    % Mu increment (for trainlm)
                net.trainParam.mu_max = 10^10;                  % Max Mu (for trainlm)
                net.trainParam.mu = 100;
            end
            net.layers{1}.transferFcn = 'logsig'; % Set activation function (hidden)
            net.layers{2}.transferFcn = 'logsig'; % Set activation function (output)
            net.inputs{1}.processFcns = {};                 % Disable processing for inputs
            net.outputs{2}.processFcns = {};                % Disable processing for outputs 
            net.performParam.regularization = obj.lambda;   % Regularization coefficient
            net.divideFcn = '';                             % Disable early stopping

            % Configure and initialize
            net = configure(net, dataset.X', dataset.Y');
            net = init(net);
            
            % Set custom weights
            net.IW{1} = obj.W_in_h(1:end-1, :)';
            net.LW{2} = obj.W_h_out(1:end-1, :)';
            net.b{1} = obj.W_in_h(end, :)';
            net.b{2} = obj.W_h_out(end, :)';
        end
        
    end
    
end

