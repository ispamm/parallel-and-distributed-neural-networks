classdef VanillaMLP < MultilayerPerceptron
    % VanillaMLP - An MLP trained with first-order gradient descent with
    % fixed step-size.
    % 'matlab_cost' can be set to true to evaluate a modified cost function
    % consistent with MATLAB's neural network, i.e.:
    %   L = ((1-lambda)/NO)*errors + lambda/W*regularizer,
    % where N is the number of exmaples, O is the number of outputs, W is
    % the number of weights.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        lambda;         % Regularization factor
        epochs;         % Number of epochs
        alpha;          % Step-size
        threshold;      % Threshold on the gradient norm
        matlab_cost;    % Boolean for modified cost function
    end
    
    methods
        function obj = VanillaMLP(name, N_hidden, varargin)
            obj = obj@MultilayerPerceptron(name, N_hidden);
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParamValue('epochs', 250);
            p.addParamValue('lambda', 0.2);
            p.addParamValue('alpha', 1);
            p.addParamValue('threshold', 10^-5);
            p.addParamValue('matlab_cost', false);
            p.parse(varargin{:});
            obj.epochs = p.Results.epochs;
            obj.lambda = p.Results.lambda;
            obj.alpha = p.Results.alpha;
            obj.threshold = p.Results.threshold;
            obj.matlab_cost = p.Results.matlab_cost;
        end
        
        function [obj, trainInfo] = train(obj, dataset)
    
            N = size(dataset.X, 1);

            obj = obj.startTimer(true);

            trainInfo.fval = zeros(obj.epochs, 1);
            trainInfo.gval = zeros(obj.epochs, 1);

            if(obj.matlab_cost)
                coeff_l = ((1-obj.lambda)/N);
                coeff_r = obj.lambda/(obj.N_weights_in+obj.N_weights_h);
            else
                coeff_l = 1;
                coeff_r = obj.lambda;
            end
            
            for n = 1:obj.epochs
               
                % Computes the gradients
                [pre, post] = obj.forwardPass(dataset.X);
                [grads_error, grads_weights] = obj.backwardPass(pre, post, dataset.X, dataset.Y);
                
                % Compute cost function and gradient norm
                trainInfo.fval(n) = coeff_l*sum((dataset.Y - post{2}).^2) + coeff_r*(norm(obj.getWeights()))^2;
                trainInfo.gval(n) = norm(coeff_l*[grads_error{1}(:); grads_error{2}(:)] + coeff_r*[grads_weights{1}(:); grads_weights{2}(:)]);
                
                % Compute descent direction
                update_in_h = coeff_l*grads_error{1} + coeff_r*grads_weights{1};
                update_h_out = coeff_l*grads_error{2} + coeff_r*grads_weights{2};
                
                % Update the weights
                obj.W_in_h = obj.W_in_h - obj.alpha*update_in_h;
                obj.W_h_out = obj.W_h_out - obj.alpha*update_h_out;

                if(trainInfo.gval(n)^2 < obj.threshold)
                    break;
                end
                
            end
            
            obj = obj.stopTimer();
            trainInfo.training_time = obj.training_time;
            
        end

    end
    
end

