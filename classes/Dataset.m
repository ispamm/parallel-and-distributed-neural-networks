classdef Dataset
    % DATASET - Class for handling dataset partitioning in the following
    % two ways:
    %   (i) Partition a dataset N times using a predefined strategy (see
    %   folder 'ParititionStrategy'), this can be used to have consistent
    %   partitioning across multiple algorithms.
    %   (ii) Partition a dataset across a network of L agents in an uniform
    %   way.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        name;   % The name of the dataset
        X;      % Matrix of input values (examples x features)
        Y;      % Matrix of output values (examples x outputs)
        task;               % Task (see Tasks)
        currentPartition;   % Id of current active partition
        partitions;         % Cell array of partitiongs
        N_nodes;            % Number of nodes in a network
        distrPartition;     % Partitions in the distributed case
    end
    
    methods
        function obj = Dataset(name, X, Y, task)
            obj.name = name;
            obj.X = X;
            obj.Y = Y;
            obj.task = task;
        end
        
        function obj = normalize(obj, l_range, u_range)
            % Normalize using an affine transformation in a given range.
            obj.X = mapminmax(obj.X',l_range, u_range);
            obj.X = obj.X';
        end
        
        function obj = generateNPartitions(obj, N, partitionStrategy)
            % Generate N partitions of the dataset using a given
            % partitioning strategy.
 
            obj.partitions = cell(N, 1);
            obj.currentPartition = 1;
            
            for ii=1:N
                obj.partitions{ii} = partitionStrategy.partition(obj.Y);
            end
            
        end
        
        function obj = generateSinglePartition(obj, partitionStrategy)
            % Commodity method for generating a single partition
           obj = obj.generateNPartitions(1, partitionStrategy);
        end
        
        function f = folds(obj)
            % Get the number of folds for the current partition
            f = obj.partitions{obj.currentPartition}.getNumFolds();
        end
        
        function obj = setCurrentPartition(obj,ind)
            % Set the current partition's index
            obj.currentPartition = ind;
        end
        
        function [trainData, testData] = getFold(obj, ii)
            % Divide the data according to current partition, ith fold

            % Get current partition
            partitionStrategy = obj.partitions{obj.currentPartition};
            partitionStrategy = partitionStrategy.setCurrentFold(ii);
            
            % Get training and test indexes
            trainInd = partitionStrategy.getTrainIndexes();
            testInd = partitionStrategy.getTestIndexes();

            % Partition the data
            trainData = Dataset(obj.name, obj.X(trainInd,:), obj.Y(trainInd,:), obj.task);
            testData = Dataset(obj.name, obj.X(testInd,:), obj.Y(testInd,:), obj.task);
            
        end
        
        function obj = distributeDataset(obj, PartitionStr)
            % Distribute the dataset according to given partition
            obj.N_nodes = PartitionStr.getNumFolds;
            obj.distrPartition = PartitionStr.partition(obj.Y);
        end
        
        function localDataset = getLocalPart(obj, ii)
            % Get the dataset for the ith node
            partitionStrategy = obj.distrPartition;
            partitionStrategy = partitionStrategy.setCurrentFold(ii);

            % The data for the ith agent corresponds to the indexes of the 
            % ith testing fold
            localInd = partitionStrategy.getTestIndexes();
            localDataset = Dataset(obj.name, obj.X(localInd, :), obj.Y(localInd, :), obj.task);
        end
        
    end
    
end

