classdef DGradMLP < VanillaMLP
    % DGradMLP - Distributed gradient training for MLP over networks.
    % At every step, each agent takes a single gradient descent step,
    % followed by an averaging of its own estimate with respect to the
    % estimates of its neighbors.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        alpha0;     % Initial step-size
        epsilon;    % Parameter for decreasing the step-size
        savedNets;  % Cell array with local neural networks
    end
    
    methods
        function obj = DGradMLP(name, N_hidden, varargin)
            obj = obj@VanillaMLP(name, N_hidden, varargin{:});
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParamValue('alpha0', 1);
            p.addParamValue('epsilon', 0.5);
            p.parse(varargin{:});
            obj.alpha0 = p.Results.alpha0;
            obj.epsilon = p.Results.epsilon;
        end
        
        function obj = init(obj, d)
            obj = obj.init@MultilayerPerceptron(d, 'gaussian');
        end
        
        function [obj, trainInfo] = train(obj, dataset, net)
            
            obj.training_time = 0;
            obj = obj.startTimer();
            
            % Number of agents
            L = net.N;
            
            % Coefficients
            if(obj.matlab_cost)
                coeff_l = ((1-obj.lambda)/(size(dataset.X, 1)*obj.N_outputs));
                coeff_r = (obj.lambda/(obj.N_weights_in+obj.N_weights_h));
            else
                coeff_l = 1;
                coeff_r = obj.lambda;
            end
            
            % Initialize networks
            obj.savedNets = cell(L, 1);
            theta = zeros(obj.N_weights_in+obj.N_weights_h, L);
            for k = 1:L
                obj.savedNets{k} = obj.init(dataset.getLocalPart(k));
                theta(:, k) = [obj.savedNets{k}.W_in_h(:); obj.savedNets{k}.W_h_out];
            end
            obj = obj.reshapeWeights(mean(theta, 2));

            % Initialize output statistics
            trainInfo.fval = zeros(obj.epochs, 1);
            trainInfo.gval = zeros(obj.epochs, 1);
            trainInfo.disagreement = zeros(obj.epochs, L);
            
            alpha = obj.alpha0;
            
            for n = 1:obj.epochs
               
                % Compute disagreement
                thetaMean = mean(theta, 2);
                for k = 1:L
                    trainInfo.disagreement(n, k) = norm(theta(:, k) - thetaMean)^2;
                end
                
                % Save current objective function and gradient norm
                [pre, post] = obj.forwardPass(dataset.X);
                [g_l, g_r] = obj.backwardPass(pre, post, dataset.X, dataset.Y);
                trainInfo.fval(n) = coeff_l*sum((dataset.Y - post{2}).^2) + coeff_r*norm([obj.W_in_h(:); obj.W_h_out(:)])^2;
                trainInfo.gval(n) = norm(coeff_l*[g_l{1}(:); g_l{2}(:)] + coeff_r*[g_r{1}(:); g_r{2}(:)], inf);
                
                % Update local networks
                for k = 1:L
                    d_local = dataset.getLocalPart(k);
                    [pre, post] = obj.savedNets{k}.forwardPass(d_local.X);
                    [g_l, g_r] = obj.savedNets{k}.backwardPass(pre, post, d_local.X, d_local.Y);
                    dir = coeff_l*[g_l{1}(:); g_l{2}(:)] + coeff_r*[g_r{1}(:); g_r{2}(:)];
                    theta(:,k) = theta(:,k) - alpha*dir;
                end
                
                % Perform consensus
                theta = theta*net.W;
                
                % Set new weights
                for k = 1:L
                    obj.savedNets{k} = obj.savedNets{k}.reshapeWeights(theta(:, k));
                end
                
                % Set new weights for general evaluation
                thetaMean = mean(theta, 2);
                obj = obj.reshapeWeights(thetaMean);
                
                % Update step size
                alpha = alpha*(1-obj.epsilon*alpha);
                
            end
            
            % Save training time
            obj = obj.stopTimer(L);
            trainInfo.training_time = obj.training_time;
            
        end
        
    end
    
end

