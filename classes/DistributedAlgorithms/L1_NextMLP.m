classdef L1_NextMLP < L2_NextMLP
    % L1_NextMLP - Neural network trained using SCA in a distributed
    % fashion, with squared errors and L1 regularization on the weights.
    % The convex surrogate is found by linearizing only the neural network
    % model. The resulting l1 minimization problem can be solved via CVX
    % (if installed), or using any one of the functions contained in the
    % L1General package (folder 'functions').
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        internalThreshold;  % Internal threshold for solving the L1 minimization problem
        internalIterations; % Number of iterations for solving the L1 minimization problem
        solver;             % Solver to be used (CVX or one from the L1General library)
        options;
    end
    
    methods
        
        function obj = L1_NextMLP(name, N_layers, varargin)
            obj = obj@L2_NextMLP(name, N_layers, varargin{:});
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParamValue('internalThreshold', 10^-8);
            p.addParamValue('internalIterations', 400);
            p.addParamValue('solver', 'L1General2_PSSgb');
            p.parse(varargin{:});
            obj.internalThreshold = p.Results.internalThreshold;
            obj.internalIterations = p.Results.internalIterations;
            obj.solver = p.Results.solver;
            obj.options.verbose = 0;
            obj.options.order = 1;
            obj.options.progTol = obj.internalThreshold;
            obj.options.maxIter = obj.internalIterations;
        end
        
        function obj = initSCA(obj, dataset, ~)
            obj = initSCA@L2_NextMLP(obj, dataset, obj.processors);
            obj.eye_preComputed = obj.tau*eye(obj.N_weights_in + obj.N_weights_h);
        end
        
        function obj = finalizeSCA(obj, ~, ~)
            % Apply a threshold on the final weights
            w_mean = zeros(obj.N_weights_in + obj.N_weights_h, 1);
            for k = 1:length(obj.savedNets)
                w = obj.savedNets{k}.getWeights();
                w_mean = w_mean + w;
            end
            w_mean = w_mean./length(obj.savedNets);
            w_mean(abs(w_mean) < 10^-5) = 0;
            obj = obj.reshapeWeights(w_mean);
        end
        
        function fval = computeFVal(obj, ~, Y, ~, post, thetaAvg)
            
            % Compute objective function
            fval =  sum(sum((Y - post{2}).^2)) + obj.lambda*norm(thetaAvg, 1);
            
        end
        
        function [grads_l, grads_r] = computeGradients(obj, X, Y, pre, post, ~)
            
            % Perform backward pass over network
            grads_l = obj.backwardPass(pre, post, X, Y);
            
            % Get (pseudo)gradient for the regularization term
            grads_r = cell(2, 1);
            grads_r{1} = obj.lambda*sign(obj.W_in_h);
            grads_r{2} = obj.lambda*sign(obj.W_h_out);
            
        end
        
        function [thetaHat, obj] = solveSurrogateFunction(obj, X, Y, pre, post, thetaCurr, pi)
           
           % Compute A and b
           [A, b, ~, obj] = obj.compute_AB(X, Y, pre, post, thetaCurr, obj.processors(end), pi);

            if(obj.processors == 1)
                
                N_weights = obj.N_weights_in + obj.N_weights_h;
                
                % Solve the l1 minimization problem
                if(strcmp(obj.solver, 'cvx'))
                    if(~exist('cvx_version', 'file'))
                        error('CVX must be installed for using the ''cvx'' solver in L1_ScaMLP.');
                    end
                    cvx_begin quiet
                    variable theta(N_weights)
                    minimize(theta'*A*theta - 2*b'*theta + obj.lambda*norm(theta, 1))
                    cvx_end
                    thetaHat = theta;
                else
                    fcn = @(w) L1_NextMLP.evalSurrogate(w, A, b);
                    optFcn = str2func(obj.solver);
                    thetaHat = optFcn(fcn, thetaCurr, obj.lambda*ones(N_weights, 1), obj.options);
                end
            
            else
                
                obj = obj.stopAndStartTimer();
                thetaHat = zeros(obj.N_weights_in + obj.N_weights_h, 1);

                optFcn = str2func(obj.solver);
                
                % Solve the l1 minimization problems
                for k = obj.processors
                    
                    N_weights = sum(obj.idx.test(k));
                    
                    if(strcmp(obj.solver, 'cvx'))
                        if(~exist('cvx_version', 'file'))
                            error('CVX must be installed for using the ''cvx'' solver in L1_ScaMLP.');
                        end
                        cvx_begin quiet
                        variable theta(N_weights)
                        minimize(theta'*A{k}*theta - 2*b{k}'*theta + obj.lambda*norm(theta, 1))
                        cvx_end
                        thetaHat(obj.idx.test(k)) = theta;
                    else
                        fcn = @(w) L1_NextMLP.evalSurrogate(w, A{k}, b{k});
                        thetaHat(obj.idx.test(k)) = optFcn(fcn, thetaCurr(obj.idx.test(k)), obj.lambda*ones(N_weights, 1), obj.options);
                    end
                end
                
                obj = obj.stopAndStartTimer(obj.processors(end));
                
            end
                
        end
        
    end
    
    methods(Static)
        function [fval, gval] = evalSurrogate(w, A, b)
            % Evaluate the surrogate function and its gradient.
            fval = w'*A*w - 2*b'*w;
            gval = 2*A*w - 2*b;
        end
    end
    
end

