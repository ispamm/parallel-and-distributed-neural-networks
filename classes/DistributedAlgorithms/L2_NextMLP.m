classdef L2_NextMLP < NextMLP
    % L2_NextMLP - Neural network trained using SCA in a distributed
    % fashion, with squared errors and L2 regularization on the weights.
    % The convex surrogate is found by linearizing only the neural network
    % model. The resulting problem can be solved in closed form via a
    % matrix inversion.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        lambda;          % Regularization coefficient
        matlab_cost;     % Boolean to use MATLAB modified cost function
        coeff_l;         % Coefficient for error term
        coeff_r;         % Coefficient for regularization term
        eye_preComputed; % Precomputed identity matrix
        processors;      % Number of (virtual) processors to use
        proc_cell;       % Same as before, but cell array
        idx;             % Store the assigment of weights to processors
    end
    
    methods
        
        function obj = L2_NextMLP(name, N_hidden, varargin)
            obj = obj@NextMLP(name, N_hidden, varargin{:});
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParamValue('lambda', 0.2);
            p.addParamValue('matlab_cost', false);
            p.addParamValue('processors', 1);
            p.parse(varargin{:});
            obj.lambda = p.Results.lambda;
            obj.matlab_cost = p.Results.matlab_cost;
            obj.processors = p.Results.processors;
        end
        
        function obj = initSCA(obj, dataset, net)
            
            % Eventually modify the tau parameter
            if(obj.matlab_cost)
                obj.tau = obj.tau/(obj.N_weights_in + obj.N_weights_h);
            end
            
            % Partition the weights
            if(nargin == 3 && obj.processors > 1)
                obj.idx = cvpartition(obj.N_weights_in + obj.N_weights_h, 'KFold', obj.processors);
                obj.processors = 1:obj.processors;
            else
                obj.idx.test = @(~) true(obj.N_weights_in + obj.N_weights_h, 1);
                obj.idx.training = @(~) false(obj.N_weights_in + obj.N_weights_h, 1);
                obj.processors = 1;
            end
            
            N = size(dataset.X, 1);
            obj.proc_cell = num2cell(obj.processors);
            
            % Auxiliary variables
            if(obj.matlab_cost)
                obj.coeff_l = ((1-obj.lambda)/(N*obj.N_outputs));
                obj.coeff_r = (obj.lambda/(obj.N_weights_in+obj.N_weights_h));
            else
                obj.coeff_l = 1;
                obj.coeff_r = obj.lambda;
            end
            
            % Pre-computed identity matrix
            obj.eye_preComputed = (obj.coeff_r+obj.tau)*eye(obj.N_weights_in + obj.N_weights_h);
        end
        
        function fval = computeFVal(obj, ~, Y, ~, post, thetaCurr)
            
            % Compute objective function
            fval =  obj.coeff_l*sum(sum((Y - post{2}).^2)) + obj.coeff_r*(norm(thetaCurr))^2;
            
        end
        
         function [grads_l, grads_r] = computeGradients(obj, X, Y, pre, post, ~)
            
            % Perform backward pass over network
            [grads_l, grads_r] = obj.backwardPass(pre, post, X, Y);
            
            grads_l{1} = obj.coeff_l*grads_l{1};
            grads_l{2} = obj.coeff_l*grads_l{2};
            grads_r{1} = obj.coeff_r*grads_r{1};
            grads_r{2} = obj.coeff_r*grads_r{2};
            
        end
        
        function [thetaHat, obj] = solveSurrogateFunction(obj, X, Y, pre, post, thetaCurr, pi)
            
            % Compute A and b matrices
            [A, b, ~, obj] = obj.compute_AB(X, Y, pre, post, thetaCurr, obj.processors(end), pi);
            
            % Solve the surrogate problem
            [thetaHat, obj] = obj.solveWithAB(A, b, obj.processors(end), thetaCurr);
            
        end
        
        function [A_out, b_out, J, obj] = compute_AB(obj, X, Y, pre, post, thetaCurr, L, pi)
            
            % Compute the Jacobian weight matrix
            J = obj.computeJacobian(pre, post, X);
            
            % Compute the r vector
            r = reshape((Y-post{2})',1,[])' + J*thetaCurr;
            
            if(L > 1)

                
                obj = obj.stopAndStartTimer();
                
                A_out = cell(L, 1);
                b_out = cell(L, 1);
                
                for k = obj.processors
                    
                    % Compute row-wise block of A
                    A_block = J(:, obj.idx.test(k))'*J;
                    
                    % Compute kth A matrix
                    A_out{k} = obj.coeff_l*A_block(:, obj.idx.test(k)) + obj.eye_preComputed(obj.idx.test(k),obj.idx.test(k));
                    
                    % Compute kth b vector
                    b_out{k} = obj.coeff_l*((r'*J(:, obj.idx.test(k)))' - A_block(:, obj.idx.training(k))*thetaCurr(obj.idx.training(k))) + obj.tau*thetaCurr(obj.idx.test(k)) - 0.5*pi(obj.idx.test(k));

                end
                
                obj = obj.stopAndStartTimer(L);
                
            else
                
                % Compute final A matrix and b vector
                A_out = obj.coeff_l*(J'*J) + obj.eye_preComputed;
                b_out = obj.coeff_l*(r'*J)' + obj.tau*thetaCurr - 0.5*pi;
                
            end
            
        end
        
        function [thetaHat, obj] = solveWithAB(obj, A, b, L, thetaCurr)
            % Add the regularization term
            if(L > 1)
                obj = obj.stopAndStartTimer();
                
                thetaHat = zeros(length(thetaCurr), 1);

                for k = 1:L
                    thetaHat(obj.idx.test(k)) = A{k}\b{k};
                end
                
                obj = obj.stopAndStartTimer(L);
            else
                thetaHat = A\b;
            end
        end
        
    end
    
end
