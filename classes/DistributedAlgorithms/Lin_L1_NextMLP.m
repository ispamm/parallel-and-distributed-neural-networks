classdef Lin_L1_NextMLP < L1_NextMLP
    % L1_NextMLP - Neural network trained using SCA in a distributed
    % fashion, with squared errors and L1 regularization on the weights.
    % The surrogate function is obtained by linearizing all the squared
    % error function. The final surrogate can be expressed in closed form
    % using a soft thresholding operator.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
    end
    
    methods
        
        function obj = Lin_L1_NextMLP(name, N_layers, varargin)
            obj = obj@L1_NextMLP(name, N_layers, varargin{:});
        end
        
        function [thetaHat, obj] = solveSurrogateFunction(obj, X, Y, pre, post, thetaCurr, pi)
           
            % Compute the gradients of the error term
            grads_l = obj.computeGradients(X, Y, pre, post, thetaCurr);
            grads_l = [grads_l{1}(:); grads_l{2}(:)];
            
            % Apply soft thresholding
            tau = 2*obj.tau;
            w = thetaCurr - grads_l./tau - pi./tau;
            alpha = obj.lambda/tau;
            thetaHat = max( 0, w - alpha ) - max( 0, - w - alpha );
            
        end
        
    end
    
end

