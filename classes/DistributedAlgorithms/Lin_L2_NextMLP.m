classdef Lin_L2_NextMLP < L2_NextMLP
    % L2_NextMLP - Neural network trained using SCA in a distributed
    % fashion, with squared errors and L2 regularization on the weights.
    % The surrogate function is obtained by linearizing all the squared
    % error function. The final surrogate can be expressed in closed form
    % without matrix inversions.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
    end
    
    methods
        
        function obj = Lin_L2_NextMLP(name, N_layers, varargin)
            obj = obj@L2_NextMLP(name, N_layers, varargin{:});
        end
        
        function obj = initSCA(obj, dataset, net)
            obj = initSCA@L2_NextMLP(obj, dataset, net);
        end
        
        function [thetaHat, obj] = solveSurrogateFunction(obj, X, Y, pre, post, thetaCurr, pi)

            if(obj.processors == 1)
                % Compute gradients
                grads_l = obj.computeGradients(X, Y, pre, post, thetaCurr);
                grads_l = [grads_l{1}(:); grads_l{2}(:)];
                % Compute the new thetaHat
                thetaHat = (1/(obj.tau + 2*obj.lambda))*(obj.tau*thetaCurr - grads_l - pi);
            else
                thetaHat = zeros(obj.N_weights_in + obj.N_weights_h, 1);
                obj = obj.stopAndStartTimer();
                % Compute gradients
                grads_l = obj.computeGradients(X, Y, pre, post, thetaCurr);
                grads_l = [grads_l{1}(:); grads_l{2}(:)];
                % Compute the new thetaHat for each processor
                for k = obj.processors
                    thetaHat(obj.idx.test(k)) = (1/(obj.tau + 2*obj.lambda))*(obj.tau*thetaCurr(obj.idx.test(k)) - grads_l(obj.idx.test(k)) - pi(obj.idx.test(k)));
                end
                obj = obj.stopAndStartTimer(obj.processors(end));
            end

        end
        
    end
    
end
