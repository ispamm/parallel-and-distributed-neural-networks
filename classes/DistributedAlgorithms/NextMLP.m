classdef NextMLP < MultilayerPerceptron
    % NextMLP - Abstract class for training a neural network in a
    % distributed fashion. Each implementation must define appropriate
    % methods in order to  (i) evaluate the cost function, (ii) evaluate 
    % the gradients, and (iii) solve the surrogate cost function.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        epochs;         % Number of epochs
        alpha0;         % Initial step-size
        epsilon;        % Decreasing factor for the step-size
        tau;            % Regularization coefficient for the strongly convex surrogate cost function
        threshold;      % Internal threshold on the gradient's norm
        curr_iteration; % Current iteration
        thetaCurr;      % Matrix of current weights
        vCurr;          % Matrix of current auxiliary variables
        piCurr;         % Matrix of current values for pi
        savedNets;      % Cell array storing the local neural networks
    end
    
    methods
        
        function obj = NextMLP(name, N_hidden, varargin)
            obj = obj@MultilayerPerceptron(name, N_hidden);
            p = inputParser;
            p.addParamValue('epochs', 250);
            p.addParamValue('alpha0', 1);
            p.addParamValue('epsilon', 0.5);
            p.addParamValue('threshold', 10^-6);
            p.addParamValue('tau', 0);
            p.KeepUnmatched = true;
            p.parse(varargin{:});
            obj.epochs = p.Results.epochs;
            obj.alpha0 = p.Results.alpha0;
            obj.epsilon = p.Results.epsilon;
            obj.tau = p.Results.tau;
            obj.threshold = p.Results.threshold;
            obj.curr_iteration = 1;
        end
        
        function [obj, trainInfo] = train(obj, dataset, net)

            % Initialize appropriate parameters
            obj = obj.initSCA(dataset, net);
            L = net.N;

            % Initialize statistics
            trainInfo.fval = zeros(obj.epochs, 1);
            trainInfo.gval = zeros(obj.epochs, 1);
            trainInfo.dir = zeros(obj.epochs, 1);
            trainInfo.disagreement = zeros(obj.epochs, L);
            
            % Initialize step size
            alpha = zeros(obj.epochs, 1);
            alpha(1) = obj.alpha0;
            
            N_weights = obj.N_weights_in + obj.N_weights_h;
            
            % Commodity matrix for storing the gradients of the loss
            % function
            grads_l = zeros(N_weights, L);
            dirCurr = zeros(N_weights, L);
            
            % Commodity matrix for storing the new variables v
            vCurrNew = zeros(N_weights, L);
            
            % Initialize auxiliary variable
            psi = zeros(N_weights, L);
            
            % Initialize the local weights
            obj.savedNets = cell(L, 1);
            obj.thetaCurr = zeros(N_weights, L);
            for k=1:L
                obj.savedNets{k} = obj.init(dataset);
                obj.savedNets{k}.training_time = 0;
                obj.thetaCurr(:,k) = obj.savedNets{k}.getWeights();
            end

            % Initialize the auxiliary variables vk and pik
            obj = obj.startTimer(true);
            obj.vCurr = zeros(N_weights, L);
            obj.piCurr = zeros(N_weights, L);
            for k = 1:L
                grads_l(:, k) = obj.savedNets{k}.computeLossGradients(dataset.getLocalPart(k));
                obj.vCurr(:, k) = grads_l(:, k);
                obj.piCurr(:, k) = (L-1)*obj.vCurr(:, k);
            end
            obj = obj.stopTimer(L);
            
            for n = 1:obj.epochs
                
                % Set current iteration
                obj.curr_iteration = n;

                % Compute disagreement
                theta_mean = mean(obj.thetaCurr, 2);
                for k = 1:L
                    trainInfo.disagreement(n, k) = norm(obj.thetaCurr(:, k) - theta_mean)^2;
                end
                
                % Compute objective functions and gradient norms
                % (for evaluation purposes)
                obj = obj.reshapeWeights(mean(obj.thetaCurr, 2));
                [pre, post] = obj.forwardPass(dataset.X);
                trainInfo.fval(n) = obj.computeFVal(dataset.X, dataset.Y, pre, post, mean(obj.thetaCurr, 2));
                [gmean_l, gmean_r] = obj.computeGradients(dataset.X, dataset.Y, pre, post, mean(obj.thetaCurr, 2));
                trainInfo.gval(n) = norm([gmean_l{1}(:); gmean_l{2}(:)] + [gmean_r{1}(:); gmean_r{2}(:)], inf);
                
                % Check termination condition
                if (trainInfo.gval(n) <= obj.threshold)
                    break;
                end
                
                % Compute new step size
                alpha(n+1) = alpha(n)*(1-obj.epsilon*alpha(n));
                
                for k = 1:L

                    % Solve inner optimization problem for thetakHat[n]
                    obj.savedNets{k} = obj.savedNets{k}.startTimer();
                    d_local = dataset.getLocalPart(k);
                    [pre, post] = obj.savedNets{k}.forwardPass(d_local.X);
                    [thetaHat, obj.savedNets{k}] = obj.savedNets{k}.solveSurrogateFunction(d_local.X, d_local.Y, pre, post, obj.thetaCurr(:, k), obj.piCurr(:,k));

                    % Update psi
                    dirCurr(:, k) = (thetaHat - obj.thetaCurr(:, k));
                    psi(:, k) = obj.thetaCurr(:, k) + alpha(n+1)*dirCurr(:, k);
                    obj.savedNets{k} = obj.savedNets{k}.stopTimer();
                    
                end
                
                % Perform one consensus step on psi
                obj = obj.startTimer();
                obj.thetaCurr = psi*net.W;
                
                
                for k=1:L
                    
                    % Update v
                    obj.savedNets{k} = obj.savedNets{k}.reshapeWeights(obj.thetaCurr(:, k));
                    gk_l = obj.savedNets{k}.computeLossGradients(dataset.getLocalPart(k));
                    vCurrNew(:, k) = obj.vCurr*net.W(:, k) + gk_l - grads_l(:, k);
                    
                    % Update pi
                    obj.piCurr(:, k) = L*vCurrNew(:, k) - gk_l;
                    
                    % Update the stored gradients
                    grads_l(:, k) = gk_l;
                    
                end
                
                % Save new v
                obj.vCurr = vCurrNew;
                
                obj = obj.stopTimer(L);
                
                % Compute norm of descent direction
                if(L == 1)
                    trainInfo.dir(n) = norm(dirCurr);
                else
                    trainInfo.dir(n) = mean(sqrt(sum(abs(dirCurr).^2,2)));
                end
                
                % Compute additional statistics
                obj = obj.computeAdditionalStatistics();

            end
            
            % Set final weights
            obj = obj.reshapeWeights(mean(obj.thetaCurr, 2));
            
            % Save the training statistics
            trainInfo.alpha = alpha;
            trainInfo.training_time = 0;
            for k = 1:L
                trainInfo.training_time = trainInfo.training_time + obj.savedNets{k}.training_time;
            end
            trainInfo.training_time = trainInfo.training_time/L + obj.training_time;
            
            % Finalize the optimization process
            obj = obj.finalizeSCA(dataset, net);
            
        end
        
        function gk_l = computeLossGradients(obj, d_local)
            % Compute the gradients of the loss function with respect to a
            % local dataset
            [pre, post] = obj.forwardPass(d_local.X);
            gk_l = obj.computeGradients(d_local.X, d_local.Y, pre, post);
            gk_l = [gk_l{1}(:); gk_l{2}(:)];
        end
        
        function J = computeJacobian(obj, preActFunc, postActFunc, X)
            % Compute the weight Jacobian matrix
            
            [N, d] = size(X);
            
            % Initialize output structure
            J = zeros(obj.N_outputs*N, obj.N_weights_in + obj.N_weights_h);
            
            % Compute derivatives of activation functions
            dActFunc_output = obj.dActFunc(preActFunc{2});    % NxO
            dActFunc_hidden = obj.dActFunc(preActFunc{1});    % NxH
            
            if(obj.N_outputs > 1)
            
                % Compute gradients for output matrix
                % It has the gradients for each example in the third dimension
                grad_output = bsxfun(@times, [postActFunc{1} ones(N,1)]', permute(dActFunc_output, [3 1 2]));
                grad_output = permute(grad_output, [1 3 2]);

                % Compute (partial) gradients for hidden matrix (output part)
                partial_hidden = bsxfun(@times, [X ones(N,1)]', permute(dActFunc_hidden, [3 1 2]));
                partial_hidden = permute(partial_hidden, [2 1 3]);

                for k = 1:obj.N_outputs

                    % Utility matrix for element-wise products
                    W_h_out_repeated = repmat(obj.W_h_out(1:end-1,k)', d+1, 1);

                    % Compute (partial) gradients for hidden matrix (hidden part)
                    partial_hidden_2 = bsxfun(@times, permute(W_h_out_repeated, [3 1 2]), dActFunc_output(:,k));

                    % Compute final gradients for hidden matrix
                    grad_hidden = partial_hidden.*partial_hidden_2;

                    % Stack all gradients in resulting Jacobian
                    J(k:obj.N_outputs:end, 1:obj.N_weights_in) = reshape(grad_hidden,[],obj.N_weights_in);
                    tmp = grad_output;
                    tmp(:,1:obj.N_outputs ~= k,:) = 0;
                    C = permute(tmp,[3 1 2]);
                    J(k:obj.N_outputs:end, obj.N_weights_in+1:end) = reshape(C,[],obj.N_weights_h);

                end
            
            else
                
                % Compute gradients for output matrix
                % N x H+1
                grad_output = [postActFunc{1} ones(N,1)].* repmat(dActFunc_output, 1, obj.N_hidden + 1);

                % Compute (partial) gradients for hidden matrix (output part)
                partial_hidden = bsxfun(@times, [X ones(N,1)]', permute(dActFunc_hidden, [3 1 2]));
                partial_hidden = permute(partial_hidden, [2 1 3]);

                % Utility matrix for element-wise products
                % d+1 x H
                W_h_out_repeated = repmat(obj.W_h_out(1:end-1)', d+1, 1);

                % Compute (partial) gradients for hidden matrix (hidden part)
                partial_hidden_2 = bsxfun(@times, permute(W_h_out_repeated, [3 1 2]), dActFunc_output);
                
                % Compute final gradients for hidden matrix
                grad_hidden = partial_hidden.*partial_hidden_2;
                
                % Stack all gradients in resulting Jacobian
                J(1:end, 1:obj.N_weights_in) = reshape(grad_hidden,[],obj.N_weights_in);
                J(1:end, obj.N_weights_in+1:end) = reshape(grad_output,[],obj.N_weights_h);
                
            end
            
        end
        
        function obj = initSCA(obj, dataset, net)
            % Initialize custom objects.
        end
        
        function obj = finalizeSCA(obj, dataset, net)
            % Finalize custom objects.
        end
        
        function obj = computeAdditionalStatistics(obj, theta_mean)
            % Compute any required additional statistic (e.g. sparsity of
            % the final output vector).
        end
        
    end
    
    methods(Abstract)
        fval = computeFVal(obj, X, Y, pre, post, thetaAvg); % Compute objective function
        [grads_l, grads_r] = computeGradients(obj, X, Y, pre, post, theta); % Compute gradients of the objective function
        [thetaHat, obj] = solveSurrogateFunction(obj, X, Y, pre, post, theta_k, pi_k); % Solve surrogate function
    end
    
end

