classdef LearningAlgorithm
    % LEARNINGALGORITHM - Simple abstract class for representing a generic
    % learning algorithm.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        name;           % Name of the algorithm
        params;         % Parameters of the algorithm
        initialized;    % Whether it is initialized
        training_time;  % Internal training time
        starting_time;  % Starting time
    end
    
    methods
        function obj = LearningAlgorithm(name)
            % Constructor for LearningAlgorithm object
            obj.name = name;
            obj.params = struct();
            obj.training_time = 0;
            obj.starting_time = [];
        end
        
        function obj = startTimer(obj, reset)
            % Start (or reset) the internal timer
            if(nargin == 2 && reset)
                obj.training_time = 0;
            end
            if(~isempty(obj.starting_time))
                warning('Trying to initialize twice internal timer');
            end
            obj.starting_time = clock;
        end
        
        function obj = stopTimer(obj, L)
            % Stop the internal timer. Eventually divide the elapsed time
            % by an integer value L (e.g. number of virtual processors).
            if(nargin == 1)
                L = 1;
            end
            obj.training_time = obj.training_time + etime(clock, obj.starting_time)/L;
            obj.starting_time = [];
        end

        function obj = stopAndStartTimer(obj, L)
            % Stop the timer, than start it again
            if(nargin == 1)
                L = 1;
            end
            obj = obj.stopTimer(L);
            obj = obj.startTimer();
        end
            
    end
    
    methods (Abstract)
        % Initialize the model
        obj = init(obj, dataset, varargin);
        
        % Train the algorithm using a training set
        obj = train(obj, Dataset);
        
        % Test the algorithm using a test set
        obj = test(obj, Dataset);
    end
end

