classdef MultilayerPerceptron < LearningAlgorithm
    % MultilayerPerceptron - Basic MLP with a single hidden layer and
    % sigmoid activation functions.
    % TODO: include more than a single hidden layer, and more flexible
    % activation functions.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        N_inputs;       % Number of inputs
        N_hidden;       % Size of hidden layer
        N_outputs;      % Number of outputs
        W_in_h;         % Input-hidden weights
        W_h_out;        % Hidden-output weights 
        N_weights_in;   % Number of weights in first layer
        N_weights_h;    % Number of weights in second layer
        actFunc;        % Sigmoid activation function
        dActFunc;       % Derivative of the sigmoid activation function
    end
    
    methods
        
        function obj = MultilayerPerceptron(name, N_hidden)
            obj = obj@LearningAlgorithm(name);
            obj.N_hidden = N_hidden;
            obj.actFunc = @(s) 1./(1+exp(-s));
            obj.dActFunc = @(s) (1./(1+exp(-s))).*(1-(1./(1+exp(-s))));
        end
        
        function obj = init(obj, dataset, init_method)
            % Initialize the weights of the network, supports two methods:
            %   (i) 'gaussian' (default), initialize from Gaussian
            %   distribution with mean zero and variance 0.01.
            %   (ii) 'uniform': initialize from uniform distribution in
            %   [-1, +1].
            
            if(nargin == 2)
                init_method = 'gaussian';
            end
            
            % Save auxiliary variables
            obj.N_inputs = size(dataset.X, 2);
            obj.N_outputs = size(dataset.Y, 2);
            
            if(strcmp(init_method, 'gaussian'))
                % Input to hidden weights
                obj.W_in_h = randn(obj.N_inputs + 1, obj.N_hidden)*0.0001;
                % Hidden to output weights
                obj.W_h_out = randn(obj.N_hidden + 1, obj.N_outputs)*0.0001;
            elseif(strcmp(init_method, 'uniform'))
                % Input to hidden weights
                obj.W_in_h = rand(obj.N_inputs + 1, obj.N_hidden)*2-1;
                % Hidden to output weights
                obj.W_h_out = rand(obj.N_hidden + 1, obj.N_outputs)*2-1;
            else
                error('Unrecognized initialization method in MultilayerPerceptron.');
            end
            
            % Save the number of weights
            obj.N_weights_in = numel(obj.W_in_h);
            obj.N_weights_h = numel(obj.W_h_out);
            
            % Reset the training time
            obj.training_time = 0;
            
        end
        
        function [preActFunc, postActFunc] = forwardPass(obj, X)
            % Make a forward pass over the network, returns:
            %   preActFun: cell array of pre-activations for the two
            %   layers.
            %   postActFunc: cell array of activations for the two layers.
            
            N = size(X, 1);
            
            preActFunc = cell(2, 1);
            postActFunc = cell(2, 1);
            
            % Input-to-hidden pass
            preActFunc{1} = [X ones(N, 1)]*obj.W_in_h;
            postActFunc{1} = obj.actFunc(preActFunc{1});
            
            % Hidden-to-output pass
            preActFunc{2} = [postActFunc{1} ones(N, 1)]*obj.W_h_out;
            postActFunc{2} = obj.actFunc(preActFunc{2});
            
        end
        
        function [grads_error, grads_weights] = backwardPass(obj, preActFunc, postActFunc, X, Y)
            % Make a backward pass to compute the derivatives of a squared
            % error function. The first two inputs must be gathered from a
            % call to 'forwardPass'. The second output represents the
            % derivatives of a squared regularizer on the weights.
            
            N = size(X, 1);
            
            grads_error = cell(2, 1);
            grads_weights = cell(2, 1);
            
            % Gradients for output layer
            error = Y - postActFunc{2};
            delta = -error.*obj.dActFunc(preActFunc{2});
            grads_error{2} = 2*[postActFunc{1} ones(N, 1)]'*delta;
            
            % Gradient for input-hidden layer
            delta = delta*(obj.W_h_out').*obj.dActFunc([preActFunc{1}, ones(N, 1)]);
            grads_error{1} = 2*[X ones(N, 1)]'*delta;
            grads_error{1} = grads_error{1}(:, 1:end-1);
            
            if(nargout == 2)
                % Gradients for the L2-norm of the weights
                grads_weights{1} = 2*obj.W_in_h;
                grads_weights{2} = 2*obj.W_h_out;
            end
            
        end

        function obj = train(obj, dataset)
            % Not implemented
            error('MultilayerPerceptron training function not implemented');
        end
        
        function scores = test(obj, dataset)
            [~, scores] = obj.forwardPass(dataset.X);
            scores = scores{end};
        end
     
        function w = getWeights(obj)
            % Get a vector of parameters
            w = [obj.W_in_h(:); obj.W_h_out(:)];
        end
        
        function obj = reshapeWeights(obj, theta)
            % Set the weights from a vector
            obj.W_in_h = reshape(theta(1:obj.N_weights_in), obj.N_inputs+1, obj.N_hidden);
            obj.W_h_out = reshape(theta(obj.N_weights_in+1:end), obj.N_hidden + 1, obj.N_outputs);
        end

    end
    
end

