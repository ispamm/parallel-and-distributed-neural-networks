classdef LinearTopology < NetworkTopology
    % LinearTopology - Defines a network topology where each node is
    % connected to K/2 nodes before and K/2 nodes after it.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        K;
    end
    
    methods
        function obj = LinearTopology(N, weights_type, K)
            obj = obj@NetworkTopology(N, weights_type);
            obj.K = K;
            while (~ obj.isConnected())
                obj.A = obj.buildAdjacency(K);
            end
            obj.W = obj.buildWeights(weights_type);
        end
        
        function A = buildAdjacency(obj,K)
            A = zeros(obj.N, obj.N);
            for i = 1:obj.N
                for j = 1:(K)
                    if(i + j > obj.N)
                        continue;
                    end
                    A(i, i+j) = 1;
                    A(i+j, i) = 1;
                end
            end
        end
        
        function s = getDescription(obj)
            s = sprintf('Linear topology G(%i, &i)', obj.N, obj.K);
        end
    end
    
end


