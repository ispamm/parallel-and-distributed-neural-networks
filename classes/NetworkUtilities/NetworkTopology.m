classdef NetworkTopology
    % NetworkTopology - Representation of a network topology.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        N;  % Number of nodes
        W;  % Weights matrix
        A;  % Adjacency matrix
        weights_type;  % Weights type (string)
    end
    
    methods
        function obj = NetworkTopology(N, weights_type, varargin)
            % Constructor for NetworkTopology class
            obj.N = N;
            obj.W = zeros(N,N);
            obj.A = zeros(N,N);
            obj.weights_type = weights_type;
        end
        
        function idx = getNeighbors(obj, i)
            % Get index of neighbors of node i
            if (i <= 0 || i > obj.N)
                error('The node ID is not a valid value');
            else
                idx = find(obj.A(i,:) == 1);
            end
        end
        
        function d = getDegree(obj, i)
            % Get degree of node i
            if(nargin == 2)
                if (i <= 0 || i > obj.N)
                    error('The node ID is not a valid value');
                else
                    d = sum(obj.A(i, :));
                end
            else
                d = sum(obj.A);
            end
        end
        
        function max_d = getMaxDegree(obj)
            % Get the maximum degree
            max_d = max(sum(obj.A));
        end
        
        function b = isConnected(obj)
            % Check if the graph is connected
            S = graphconncomp(sparse(obj.A), 'Weak', true, 'Directed', false);
            b = S == 1;
        end
        
        function L = buildLaplacian(obj, options)
            % Return Laplacian matrix for the Graph
            D = diag(sum(obj.W,2));
            L = D-obj.W;
        end
        
        function W = buildWeights(obj, weights_type)
            % Set the weights matrix W based on the adjacency matrix A
            
            W = zeros(obj.N, obj.N);
            max_degree = obj.getMaxDegree();
            d = obj.getDegree();
            
            if (strcmp(weights_type, 'best_constant'))
                L = obj.buildLaplacian();
                eigs = eig(L,'nobalance');
                W = eye(obj.N) - 2*L/(eigs(2) + eigs(obj.N));
            else
                for ii = 1:obj.N
                    idx = obj.getNeighbors(ii);
                    for jj = idx
                        if(strcmp(weights_type, 'max_degree'))
                            W(ii, jj) = 1/(max_degree + 1);
                        elseif(strcmp(weights_type, 'metropolis'))
                            W(ii, jj) = 1/(max([d(ii), d(jj)]) + 1);
                        end
                    end
                    if(strcmp(weights_type, 'max_degree'))
                        W(ii, ii) = 1-length(idx)/(max_degree+1);
                    elseif(strcmp(weights_type, 'metropolis')) 
                        W(ii, ii) = 1 - sum(W(ii, :));
                    end
                end
            end
        end
    end
    
    methods (Abstract)
        % Construct the adjacency matrix A.
        A = buildAdjacency(obj);
        
        % Get description
        s = getDescription(obj);
    end
    
end

