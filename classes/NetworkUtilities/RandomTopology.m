classdef RandomTopology < NetworkTopology
    % RandomTopology - Randomly generated network topology (Erdos-Renyi
    % model).
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        p; % Probability that two nodes in the network are connected by an edge
    end
    
    methods
        function obj = RandomTopology(N, weights_type, p)
            % Constructor for the RandomTopology class
            obj = obj@NetworkTopology(N, weights_type);
            obj.p = p;
            while (~ obj.isConnected())
                obj.A = obj.buildAdjacency(p);
            end
            obj.W = obj.buildWeights(weights_type);
        end
        
        function A = buildAdjacency(obj, p)
            % Build the adjacency matrix for the topology
            A = zeros(obj.N, obj.N);
            dice = triu(rand(obj.N, obj.N));
            dice = dice + triu(dice, 1)';
            A(dice < p) = 1;
            A(logical(eye(obj.N))) = 0;
        end
        
        function s = getDescription(obj)
            % Return the description of the topology
            s = sprintf('Random graph G(%i, %.1f)', obj.N, obj.p);
        end
    end
    
end

