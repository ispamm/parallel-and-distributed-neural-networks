classdef NoPartition < PartitionStrategy
    % NoPartition - Train and test folds are the same and comprise all the
    % dataset.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        y_length;
    end
    
    methods
        function obj = NoPartition()
            obj.num_folds = 1;
        end
        
        function obj = partition(obj, Y)
            obj.y_length = size(Y,1);
            obj.setCurrentFold(1);
        end
        
        function [logTrainIdx, absTrainIdx] = getTrainIndexes(obj)
            logTrainIdx = true(obj.y_length,1);
            absTrainIdx = [1:obj.y_length]';
        end
        
        function [logTestIdx, absTestIdx] = getTestIndexes(obj)
            logTestIdx = true(obj.y_length,1);
            absTestIdx =  [1:obj.y_length]';
        end
        
        function d = getDescription(obj)
            d = sprintf('Both Training and Test sets include all data (%i samples)', obj.y_length);
        end
    end
    
end

