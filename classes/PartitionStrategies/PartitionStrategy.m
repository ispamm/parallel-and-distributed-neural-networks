classdef PartitionStrategy
    % PartitionStrategy - Class to partition a Dataset object in training
    % and test parts.
    
    % License to use and modify this code is granted freely without warranty to all, as long as the original author is
    % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
    %
    % Programmed and Copyright by Simone Scardapane:
    % simone.scardapane@uniroma1.it
    
    properties
        current_fold; % Current fold index
        num_folds;    % Number of folds in the partition
    end
    
    methods (Abstract)
        obj = partition(obj, Y);
        
        [logTrainIdx, numTrainIdx] = getTrainIndexes(obj);
        
        [logTestIdx, numTestIdx] = getTestIndexes(obj);
        
        d = getDescription(obj);
    end
    
    methods
        function n = getNumFolds(obj)
            n = obj.num_folds;
        end
        
        function obj = setCurrentFold(obj,ii)
            obj.current_fold = ii;
        end
    end
end

