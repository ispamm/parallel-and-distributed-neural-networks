% Tasks - A list of all the possible supervised learning tasks
% This is used to distinguish between appropriate preprocessing steps and
% error measures.

% License to use and modify this code is granted freely without warranty to all, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.
%
% Programmed and Copyright by Simone Scardapane:
% simone.scardapane@uniroma1.it

classdef Tasks < uint32
    
    enumeration
        R   (1), % Regression
        BC  (2), % Binary Classification
        MC  (3), % Multiclass Classification
    end
    
    methods
        function ind = subsindex(A)
            % Utility function for indexing purposes
            ind = uint32(A) - 1;
        end
    end

end