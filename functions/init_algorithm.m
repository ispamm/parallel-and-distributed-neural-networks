function [ a ] = init_algorithm( a, d, W_in_h, W_h_out )
% Custom initialization function to ensure that all networks have the same
% initial weights
a = a.init(d);
if(isa(a, 'MultilayerPerceptron'))
    a.W_in_h = W_in_h;
    a.W_h_out = W_h_out;
end

end

