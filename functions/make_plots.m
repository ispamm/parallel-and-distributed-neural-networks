% MAKE_PLOTS - Plot information for the current simulation

%% General properties for the figures
width = 3.45;                   % Width in inches
height = 2.6;                   % Height in inches
font_size = 8;                  % Fontsize
font_size_leg = 6;              % Font size (legend)
font_name = 'timessNewRoman';    % Font name
line_width = 1;                 % LineWidth

%% Plot objective functions evaluations

f = figure('name', 'Objective function');
figshift;

iter_limit = Inf;
if(iter_limit > epochs)
    iter_limit = epochs;
end
leg = {};
for k = 1:N_algorithms
    if(isfield(trainInfo{1,k}, 'fval'))
        tmp = zeros(size(trainInfo{1, k}.fval));
        for i=1:runs*kfolds
            tmpInternal = trainInfo{i, k}.fval;
            lastNonZeroIndex = find(tmpInternal);
            lastNonZeroIndex = lastNonZeroIndex(end);
            lastNonZeroValue = tmpInternal(lastNonZeroIndex);
            tmpInternal(lastNonZeroIndex:end) = lastNonZeroValue;
            tmp = tmp + tmpInternal;
        end
        tmp = tmp./(runs*kfolds);
        semilogy(1:iter_limit, tmp(1:iter_limit), 'LineWidth', line_width);
        leg{end + 1} = names{k};
        hold on;
    end
end

box on;
grid on;

%ylim([10^-2.2 10^-1.2]);

xlabel('Iteration', 'FontSize', font_size, 'FontName', font_name);
ylabel('Objective function', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend(leg, 'Location', 'NorthEast');

singlecolumn_format;

if(isempty(leg))
    close(f);
end

%% Plot gradient norm

f = figure('name', 'Gradient norm');
figshift;

iter_limit = 2500;
if(iter_limit > epochs)
    iter_limit = epochs;
end
leg = {};
for k = 1:N_algorithms
    if(isfield(trainInfo{1,k}, 'gval'))
        tmp = zeros(size(trainInfo{1, k}.gval));
        for i=1:runs*kfolds
            tmpInternal = trainInfo{i, k}.gval;
            lastNonZeroIndex = find(tmpInternal);
            lastNonZeroIndex = lastNonZeroIndex(end);
            lastNonZeroValue = tmpInternal(lastNonZeroIndex);
            tmpInternal(lastNonZeroIndex:end) = lastNonZeroValue;
            tmp = tmp + tmpInternal;
        end
        tmp = tmp./(runs*kfolds);
        semilogy(1:iter_limit, tmp(1:iter_limit), 'LineWidth', line_width);
        leg{end + 1} = names{k};
        hold on;
    end
end

box on;
grid on;

xlabel('Iteration', 'FontSize', font_size, 'FontName', font_name);
ylabel('Gradient norm', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend(leg, 'Location', 'NorthEast');

singlecolumn_format;

if(isempty(leg))
    close(f);
end

%% Plot norm of descent direction

f = figure('name', 'Descent direction (norm)');
figshift;

iter_limit = 2500;
if(iter_limit > epochs)
    iter_limit = epochs;
end
leg = {};
for k = 1:N_algorithms
    if(isfield(trainInfo{1,k}, 'dir'))
        tmp = zeros(size(trainInfo{1, k}.dir));
        for i=1:runs*kfolds
            tmpInternal = trainInfo{i, k}.dir;
            lastNonZeroIndex = find(tmpInternal);
            lastNonZeroIndex = lastNonZeroIndex(end);
            lastNonZeroValue = tmpInternal(lastNonZeroIndex);
            tmpInternal(lastNonZeroIndex:end) = lastNonZeroValue;
            tmp = tmp + tmpInternal;
        end
        tmp = tmp./(runs*kfolds);
        semilogy(1:iter_limit, tmp(1:iter_limit), 'LineWidth', line_width);
        leg{end + 1} = names{k};
        hold on;
    end
end

box on;
grid on;

xlabel('Iteration', 'FontSize', font_size, 'FontName', font_name);
ylabel('Descent direction (norm)', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend(leg, 'Location', 'NorthEast');

singlecolumn_format;

if(isempty(leg))
    close(f);
end

%% Plot disagreement

f = figure('name', 'Disagreement');
figshift;

leg = {};
for k = 1:N_algorithms
    if(isfield(trainInfo{1,k}, 'disagreement'))
        tmp = mean(trainInfo{1,k}.disagreement, 2);
        for i=2:runs*kfolds
            tmp = tmp + mean(trainInfo{i,k}.disagreement, 2);
        end
        tmp = tmp./(runs*kfolds);
        semilogy(1:epochs, tmp, 'LineWidth', line_width);
        leg{end + 1} = names{k};
        hold on;
    end
end

box on;
grid on;

xlabel('Iteration', 'FontSize', font_size, 'FontName', font_name);
ylabel('Disagreement', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend(leg, 'Location', 'NorthEast');

singlecolumn_format;

if(isempty(leg))
    close(f);
end

%% Plot training times for different numbers of nodes

f = figure('name', 'Training times');
figshift;

N_distr = length(distributed_algorithms);
N_cent = length(centralized_algorithms);
tmp = mean(times, 1);

leg = {};
for k = 1:N_cent
    plot([agents(1), agents(end)], [tmp(k), tmp(k)], '--', 'LineWidth', line_width);
    leg{end + 1} = centralized_algorithms{k}.name;
    hold on;
end
a = N_cent + 1;
for k = 1:N_distr
    plot(agents, tmp(a:a+length(agents)-1), 'LineWidth', line_width);
    leg{end + 1} = distributed_algorithms{k}.name;
    hold on;
    a = a + length(agents);
end

box on;
grid on;

xlabel('Number of nodes', 'FontSize', font_size, 'FontName', font_name);
ylabel('Training times [s]', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend(leg, 'Location', 'NorthEast');

singlecolumn_format;

if(length(agents) == 1 || (N_distr) == 0)
    close(f);
end

%% Plot classification error for different numbers of nodes

f = figure('name', 'Error');
figshift;

N_distr = length(distributed_algorithms);
N_cent = length(centralized_algorithms);
tmp = mean(errors, 1);

leg = {};
for k = 1:N_cent
    plot([agents(1), agents(end)], [tmp(k), tmp(k)], '--', 'LineWidth', line_width);
    leg{end + 1} = centralized_algorithms{k}.name;
    hold on;
end
a = N_cent + 1;
for k = 1:N_distr
    plot(agents, tmp(a:a+length(agents)-1), 'LineWidth', line_width);
    leg{end + 1} = distributed_algorithms{k}.name;
    hold on;
    a = a + length(agents);
end

box on;
grid on;

xlabel('Number of nodes', 'FontSize', font_size, 'FontName', font_name);
ylabel('Classification Error', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend(leg, 'Location', 'NorthEast');

singlecolumn_format;

if(length(agents) == 1 || (N_distr) == 0)
    close(f);
end