% PARAMS_SELECTION - Example of configuration.
%   This compares the SCA algorithm for squared loss and l2 regularization
%   on the weights with a basic distributed gradient descent procedure.

% License to use and modify this code is granted freely without warranty to all, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.
%
% Programmed and Copyright by Simone Scardapane:
% simone.scardapane@uniroma1.it

% -------------------------------------------------------------------------
% --- DATASET CONFIGURATION -----------------------------------------------
% -------------------------------------------------------------------------

% Select the dataset file (see folder 'datasets')
datasetFile = 'cancer';

runs = 1;	% Number of simulations
kfolds = 3;	% Number of folds

% -------------------------------------------------------------------------
% --- NETWORK CONFIGURATION -----------------------------------------------
% -------------------------------------------------------------------------

% Network configuration                    
agents = [10];              % Nodes in the network (can be a vector for testing multiple sizes simultaneously)
connectivity = 0.2;         % Connectivity in the networks (must be between 0 and 1)


% -------------------------------------------------------------------------
% --- ALGORITHMS CONFIGURATION --------------------------------------------
% -------------------------------------------------------------------------

epochs = 800;   % Number of epochs
threshold = 0;  % Threshold on the gradient
lambda= 10^-2;  % Regularization factor
hidden = 10;    % Number of hidden neurons

% Define the centralized algorithms
centralized_algorithms = {...
    BaselineLearningAlgorithm('B'),
};

% Define the distributed algorithms
distributed_algorithms = {...
    DGradMLP('D-MLP', hidden, 'matlab_cost', false, 'epochs', epochs, 'lambda', lambda, 'threshold', threshold, 'alpha0', 0.03, 'epsilon', 0.1),
    L2_NextMLP('1-NEXT-MLP', hidden, 'matlab_cost', false, 'epochs', epochs, 'lambda', lambda, 'threshold', threshold, 'processors', 1, 'alpha0', 0.008, 'epsilon', 0.95, 'tau', 0.01),
};