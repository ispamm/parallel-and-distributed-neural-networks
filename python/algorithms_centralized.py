# -*- coding: utf-8 -*-

import sklearn.dummy
from sklearn import metrics
import lasagne
import theano
import theano.tensor as T
import utils
import numpy as np
import scipy.optimize
from timeit import default_timer as timer

"""
This module implements a set of algorithms for training the neural network,
including baseline algorithms implemented with Lasagne and SciPy, and a 
centralized version of the successive convex approximation (SCA) algorithm.

All algorithms share the following interface. Some parameters are not used here,
but they are used in the distributed algorithms. See 'run_simulation.py' for a
general taxonomy of the algorithms.
    
1) initialize: initialize the algorithm. Parameters are as follows.
        
    INPUT
        - X/y: training input and output matrices.
        - kf: k-fold cross-validation object describing the partition of the dataset
          among the agents.
        - C: mixing matrix of the agents' network (see Eq. (2) of the paper).
        - net: LasagneNet object (see utils).
        - isClassification: boolean indicating whether this is a classification task (defined in run_simulation.py).
        
    OUTPUT
        None.
        
2) train: train the algorithm. Parameters are as follows.
    
    INPUT
        Similar to initialize, with additional 'max_epochs' for the maximum number of epochs,
        and X_tst/y_tst are the test input and output matrices for computing the evolution
        of the test accuracy.
        
    OUTPUT
        - Number of elapsed epochs.
        - Loss function at every epoch.
        - Gradient norm at every epoch.
        - Disagreement at every epoch.
        - Training time.
        - Test error at every epoch.
        - Number of scalars exchanged on the agents' network at every epoch.
        
3) predict: obtain output given a new set of data.  Parameters are as follows.

    INPUT
        - X: input values.
        - net: LasagneNet object (see utils).
        
    OUTPUT:
        -  Returns the predictions.

"""

class SklearnDummyRegressor: 
    # Wrapper class to the sklearn DummyRegressor algorithm
    def initialize(self, X, y, kf, C, net, isClassification):
        return
        
    def train(self, X, y, kf, C, net, max_epochs, X_tst, y_tst, isClassification):
        start = timer()
        if isClassification:
            self.dummy = sklearn.dummy.DummyClassifier()
            self.dummy.fit(X, y.reshape(y.shape[0],))
            return 0, 0, 0, 0, timer() - start, 1 - metrics.accuracy_score(y_tst, self.predict(X_tst, net)), 0
        else:
            self.dummy = sklearn.dummy.DummyRegressor()
            self.dummy.fit(X, y)
            return 0, 0, 0, 0, timer() - start, metrics.mean_squared_error(y_tst, self.predict(X_tst, net)), 0
        
        
    def predict(self, X, net):
        return self.dummy.predict(X)
        
class LasagneUpdate:
    # Wrapper class to the update functions of Lasagne

    def __init__(self, lambda_l2=10**-3, update_fcn=lasagne.updates.adam, l_rate=0.01):
        # lambda_l2 is the regularization coefficient
        self.lambda_l2 = lambda_l2
        self.update_fcn_lasagne = update_fcn
        self.l_rate = l_rate
    
    
    def initialize(self, X, y, kf, C, net, isClassification):
        # Define the error function depending on the problem
        if isClassification:     
            self.error_loss = lasagne.objectives.binary_crossentropy(net.output, net.target_var).sum()
        else:
            self.error_loss = lasagne.objectives.squared_error(net.output, net.target_var).sum()
        
        self.reg_loss = self.lambda_l2*0.5*lasagne.regularization.regularize_network_params(net.lasagne_net, lasagne.regularization.l2, tags={})
        self.loss = self.error_loss + self.reg_loss
        self.compute_loss_fcn = theano.function([net.input_var, net.target_var], self.loss)
        self.grads = theano.function([net.input_var, net.target_var], T.grad(self.loss, net.params))
        self.compute_grad_fcn = lambda x,y: np.concatenate([g.flatten() for g in self.grads(x,y)])
        self.update_fcn = theano.function([net.input_var, net.target_var], self.loss, updates=self.update_fcn_lasagne(self.loss, net.params, self.l_rate))
        
    def train(self, X, y, kf, C, net, max_epochs, X_tst, y_tst, isClassification):
        l_vector = np.zeros(max_epochs)
        g_vector = np.zeros(max_epochs)
        err_vector = np.zeros(max_epochs)        
        
        start = timer()
        for i in range(max_epochs):
            l_vector[i] = self.compute_loss_fcn(X, y)
            g_vector[i] = np.linalg.norm(self.compute_grad_fcn(X, y))**2
            if isClassification:            
                err_vector[i] = 1 - metrics.accuracy_score(y_tst, np.round(self.predict(X_tst, net)))
            else:
                err_vector[i] = metrics.mean_squared_error(y_tst, self.predict(X_tst, net))
            self.update_fcn(X, y)
            if g_vector[i] < 0:
                break
        return i+1, l_vector[0:i+1], g_vector[0:i+1], np.zeros(i+1), timer() - start, err_vector[0:i+1], np.zeros(i+1)
        
    def predict(self, X, net):
        return net.get_output(X)
        
class ScipyMinimize(LasagneUpdate):
    # Wrapper class for SciPy minimization algorithms

    def __init__(self, minimize_fcn='CG', lambda_l2=10**-3):
        LasagneUpdate.__init__(self, lambda_l2=lambda_l2)
        self.minimize_fcn = minimize_fcn
    
    def initialize(self, X, y, kf, C, net, isClassification):
        LasagneUpdate.initialize(self, X, y, kf, C, net, isClassification)
        self.net = net
        self.current_iter = 0
        
    def compute_obj_and_grad(self, theta, X, y, X_tst, y_tst, isClassification):
        # Ensure parameters are float32 (only needed in some optimization routines like BFGS)
        theta = theta.astype(np.float32, copy=False)
        self.net.set_parameters(theta)
        l = self.compute_loss_fcn(X, y)
        self.l_vector[self.current_iter] = l
        g = self.compute_grad_fcn(X, y)
        self.g_vector[self.current_iter] = np.linalg.norm(g)**2
        if isClassification:
            self.err_vector[self.current_iter] = 1 - metrics.accuracy_score(y_tst, np.round(self.predict(X_tst, self.net)))
        else:
            self.err_vector[self.current_iter] = metrics.mean_squared_error(y_tst, self.predict(X_tst, self.net))
        self.current_iter += 1
        if(self.current_iter >= self.l_vector.shape[0]):
            self.current_iter -= 1
            g = np.zeros(theta.shape[0])
        return l, g
    
    def train(self, X, y, kf, C, net, max_epochs, X_tst, y_tst, isClassification):
        self.l_vector = np.zeros(max_epochs)
        self.g_vector = np.zeros(max_epochs)
        self.err_vector = np.zeros(max_epochs)
        start = timer()
        res = scipy.optimize.minimize(lambda theta, *args: self.compute_obj_and_grad(theta, X, y, X_tst, y_tst, isClassification), 
                                      utils.vectorize(net.start_params), method=self.minimize_fcn, 
                                jac=True, args={'maxiter': max_epochs})
        net.set_parameters(res.x.astype(np.float32, copy=False))
        return self.current_iter, self.l_vector[0:self.current_iter], self.g_vector[0:self.current_iter], np.zeros(self.current_iter), timer() - start, self.err_vector[0:self.current_iter], np.zeros(self.current_iter)
    
class PL_SCA(LasagneUpdate):
    # PL-SCA centralized algorithm

    def __init__(self, lambda_l2=10**-4, l_rate=0.1, l_rate_epsilon=0.01, tau=0, processors=1):
        
        LasagneUpdate.__init__(self, lambda_l2=lambda_l2, l_rate=l_rate)
        # Epsilon is used to reduce the step-size at every iteration.        
        self.l_rate_epsilon = l_rate_epsilon
        self.tau = tau
        # Number of virtual processors.
        self.processors = processors
        
    def initialize(self, X, y, kf, C, net, isClassification):
        
        LasagneUpdate.initialize(self, X, y, kf, C, net, isClassification)        
        
        # Define the Jacobian symbolic function
        self.jac = theano.function([net.input_var], T.grad(T.sum(net.output), net.params))
        self.net_jacobian = lambda x: np.concatenate([J.flatten() for J in self.jac(x)])
        
        # Utility variables
        self.N_params = utils.vectorize(net.start_params).shape[0] 
        self.starting_params_vectorized = utils.vectorize(net.start_params)    
    
        # Initialize optimization structures
        self.theta_curr = np.squeeze(self.starting_params_vectorized)
        
        # Precomputed identity matrix
        self.eye_precomputed = np.eye(self.N_params, dtype=theano.config.floatX)    

   
    def train(self, X, y, kf, C, net, max_epochs, X_tst, y_tst, isClassification):
    
        l_vector = np.zeros(max_epochs)
        g_vector = np.zeros(max_epochs)    
        err_vector = np.zeros(max_epochs)
        
        start = timer()
        for i in range(max_epochs):
    
            # Set current parameters
            net.set_parameters(self.theta_curr)
        
            # Get loss and gradient norm
            l_vector[i] = self.compute_loss_fcn(X, y)
            g_vector[i] = np.linalg.norm(self.compute_grad_fcn(X, y))**2
            if isClassification:
                err_vector[i] = 1 - metrics.accuracy_score(y_tst, np.round(self.predict(X_tst, net)))
            else:
                err_vector[i] = metrics.mean_squared_error(y_tst, self.predict(X_tst, net))
        
            # Get A matrix and b vector
            A, b = self.build_Ab_matrices(net, X, y, self.theta_curr)
        
            # Get solution of surrogate problem
            theta_hat = np.linalg.solve(A, b)
            dir_k = np.squeeze(theta_hat.T - self.theta_curr)
            self.theta_curr = self.theta_curr + self.l_rate*dir_k
                
            # Update step size
            self.l_rate = self.l_rate*(1-self.l_rate_epsilon*self.l_rate) 
            
            if g_vector[i] < 0:
                break
                
        return i+1, l_vector[0:i+1], g_vector[0:i+1], np.zeros(i+1), timer() - start, err_vector[0:i+1], np.zeros(i+1)

    def build_jacobian_matrix(self, net, X):
        # Build the Jacobian weight matrix
        J = np.zeros([X.shape[0], self.N_params], dtype=theano.config.floatX)
        for i in np.arange(X.shape[0]):
            J[i,:] = self.net_jacobian(X[i,:].reshape(1, X.shape[1]))    
        # Alternative formulation (TODO: check performance).
        #J = np.asarray([self.net_jacobian(X[i].reshape(1, X.shape[1])) for i in range(X.shape[0])])
        return J
        
    def build_Ab_matrices(self, net, X, y, theta):
        # Build A and b matrices as defined in Eqs. (39)-(40) of the paper.

        # Build the Jacobian weight matrix        
        J = self.build_jacobian_matrix(net, X)         
         
        # Construct A and b matrices using J
        A = np.dot(J.T, J) + (self.lambda_l2+self.tau)*0.5*self.eye_precomputed
        r = y - net.get_output(X) + np.dot(J, theta.T).reshape([X.shape[0], 1])
        b = np.dot(J.T, r) + 0.5*self.tau*theta.reshape(self.N_params, 1)
        
        return A, b
    
    def predict(self, X, net):
        net.set_parameters(self.theta_curr)
        return net.get_output(X)