# -*- coding: utf-8 -*-

import theano
import theano.tensor as T
import utils
import numpy as np
import scipy
from timeit import default_timer as timer
from sklearn import metrics, cross_validation
from lasagne import nonlinearities
from abc import ABC, abstractmethod
import algorithms_centralized as ac
from tqdm import trange

"""
This module implements a set of distributed algorithms for training the neural
network model. The interface of each class is defined in the header of the module
'algorithms_centralized'. For a taxonomy of the classes, see the header of
'run_simulation.py'.
"""

class DistGrad(ac.LasagneUpdate):
    # Distributed gradient procedure (see Sec. 6.4 of the paper)

    def __init__(self, lambda_l2=10**-3, l_rate=0.1, l_rate_epsilon=0.1):
        ac.LasagneUpdate.__init__(self, lambda_l2=lambda_l2, l_rate=l_rate)
        self.l_rate_epsilon = l_rate_epsilon
        
    def initialize(self, X, y, kf, C, net, isClassification):
        ac.LasagneUpdate.initialize(self, X, y, kf, C, net, isClassification)
        
        # Utility functions
        self.compute_error_grad_fcn = theano.function([net.input_var, net.target_var], T.grad(self.error_loss, net.params))
        self.compute_reg_grad_fcn = theano.function([], T.grad(self.reg_loss, net.params))        
        
        # Utility variables
        self.N_params = utils.vectorize(net.start_params).shape[0] 
        self.starting_params_vectorized = utils.vectorize(net.start_params)    
    
        # Initialize optimization structures
        self.theta_curr = np.repeat(self.starting_params_vectorized.reshape(1, self.N_params), C.shape[0], axis=0)
        
    def train(self, X, y, kf, C, net, max_epochs, X_tst, y_tst, isClassification):
        
        l_vector = np.zeros(max_epochs)
        g_vector = np.zeros(max_epochs)
        err_vector = np.zeros(max_epochs)        
        dis_vector = np.zeros(max_epochs)        
        
        start = timer()
        
         # Initialize auxiliary variable
        psi = np.zeros((C.shape[0], self.N_params), dtype=theano.config.floatX)            
        
        for i in range(max_epochs):
            
            # Get statistics on current epoch
            theta_mean = self.theta_curr.mean(axis=0)
    
            net.set_parameters(theta_mean)
            l_vector[i] = self.compute_loss_fcn(X, y)
            g_vector[i] = np.linalg.norm(self.compute_grad_fcn(X, y))**2
            
            if isClassification:
                err_vector[i] = 1 - metrics.accuracy_score(y_tst, np.round(self.predict(X_tst, net)))
            else:
                err_vector[i] = metrics.mean_squared_error(y_tst, self.predict(X_tst, net))
        
            # Compute current disagreement     
            for k in np.arange(C.shape[0]):
                dis_vector[i] += np.linalg.norm(self.theta_curr[k,:] - theta_mean)
                
            for k,(tr_idx,tst_idx) in zip(np.arange(C.shape[0]), kf):
            
                # Set parameters
                net.set_parameters(self.theta_curr[k,:])
                
                # Perform local adaptation
                psi[k,:] = self.theta_curr[k,:] - self.l_rate*utils.vectorize(self.compute_error_grad_fcn(X[tst_idx,:], y[tst_idx])) -\
                self.l_rate*utils.vectorize(self.compute_reg_grad_fcn())/C.shape[0]
        
            # Perform first consensus step
            self.theta_curr = np.dot(C, psi)
          
            # Update step size
            self.l_rate = self.l_rate*(1-self.l_rate_epsilon*self.l_rate)    
        
        dis_vector /= C.shape[0]
        return i+1, l_vector, g_vector, dis_vector, (timer() - start)/C.shape[0], err_vector, np.ones(max_epochs)*self.N_params
        
    def predict(self, X, net):
        net.set_parameters(self.theta_curr.mean(axis=0))
        return net.get_output(X)
    
class NEXT(ac.PL_SCA, ABC):
    # Abstract class for implementing NEXT versions    
    
    def initialize(self, X, y, kf, C, net, isClassification):
        ac.PL_SCA.initialize(self, X, y, kf, C, net, isClassification)

        # Initialize optimization structures
        self.theta_curr = np.repeat(self.starting_params_vectorized.reshape(1, self.N_params), C.shape[0], axis=0)        
        self.v_curr = np.zeros((C.shape[0], self.N_params), dtype=theano.config.floatX)
        self.pi_curr = np.zeros((C.shape[0], self.N_params), dtype=theano.config.floatX)
        self.grads_l = np.zeros((C.shape[0], self.N_params), dtype=theano.config.floatX)
        
        for k,(tr_idx,tst_idx) in zip(np.arange(C.shape[0]), kf):    
            self.grads_l[k,:] = self.compute_error_grad(X, y, tst_idx)
            self.v_curr[k,:] = self.grads_l[k,:]
            self.pi_curr[k, :] = (C.shape[0]-1)*self.v_curr[k,:]
   
    def train(self, X, y, kf, C, net, max_epochs, X_tst, y_tst, isClassification):
    
        l_vector = np.zeros(max_epochs)
        g_vector = np.zeros(max_epochs)
        err_vector = np.zeros(max_epochs)        
        dis_vector = np.zeros(max_epochs)        
        
        elapsed_time = 0
        start = timer()
        
         # Initialize auxiliary variables
        psi = np.zeros((C.shape[0], self.N_params), dtype=theano.config.floatX)            
        v_curr_new = np.zeros((C.shape[0], self.N_params))  
        
        for i in trange(max_epochs, desc='\tProgress', unit=" epochs", ncols=70):    
    
            # Get statistics on current epoch
            theta_mean = self.theta_curr.mean(axis=0)
            net.set_parameters(theta_mean)
            l_vector[i], g_vector[i] = self.get_statistics(X, y, C, net)
            if isClassification:
                err_vector[i] = 1 - metrics.accuracy_score(y_tst, np.round(self.predict(X_tst, net)))
            else:
                err_vector[i] = metrics.mean_squared_error(y_tst, self.predict(X_tst, net))
        
            # Compute current disagreement
            for k in np.arange(C.shape[0]):
                dis_vector[i] += np.linalg.norm(self.theta_curr[k,:] - theta_mean)
                
            for k,(tr_idx,tst_idx) in zip(np.arange(C.shape[0]), kf):
            
                # Set parameters
                net.set_parameters(self.theta_curr[k,:])
                    
                # Solve surrogate problem
                if self.processors == 1:
                    start = timer()
                    theta_hat, t = self.solve_surrogate_problem(k, X, y, tst_idx, net)
                    elapsed_time += t
                else:
                    elapsed_time += (timer() - start)
                    theta_hat, t = self.solve_surrogate_problem_parallel(k, X, y, tst_idx, net)
                    elapsed_time += t
                    start = timer()
                
                # Perform convex combination
                psi[k,:] = self.theta_curr[k,:] + self.l_rate*(theta_hat.T - self.theta_curr[k,:])
        
            # Perform first consensus step
            self.theta_curr = np.dot(C, psi)
            
            # Perform optimization of pi variables
            for k,(tr_idx,tst_idx) in zip(np.arange(C.shape[0]), kf):
                
                # Set parameters
                net.set_parameters(self.theta_curr[k,:])
                    
                # Update auxiliary variable vi
                gk_l = self.compute_error_grad(X, y, tst_idx)
                v_curr_new[k,:] = np.dot(C[k,:], self.v_curr) + gk_l - self.grads_l[k,:]
                
                # Update pi
                self.pi_curr[k,:] = C.shape[0]*v_curr_new[k,:] - gk_l
                
                # Update the stored gradients
                self.grads_l[k,:] = gk_l
          
            # Update v_curr with new values
            self.v_curr[:] = v_curr_new     
          
            # Update step size
            self.l_rate = self.l_rate*(1-self.l_rate_epsilon*self.l_rate)    
            
            if g_vector[i] < 0:
                break                    
        
        print("", flush=True)
        dis_vector /= C.shape[0]
        return i+1, l_vector[0:i+1], g_vector[0:i+1], dis_vector[0:i+1], (timer() - start + elapsed_time)/C.shape[0], err_vector[0:i+1], np.ones(i+1)*self.N_params*2
        #return i+1, l_vector[0:i+1], g_vector[0:i+1], dis_vector[0:i+1], elapsed_time, err_vector[0:i+1], np.ones(i+1)*self.N_params*2
        
    def predict(self, X, net):
        net.set_parameters(self.theta_curr.mean(axis=0))
        return net.get_output(X)
        
    @abstractmethod
    def compute_error_grad(self, X, y):
        # Compute gradients of the error function (nabla g_i in the paper)
        pass
    
    @abstractmethod
    def get_statistics(self, X, y, C, net):
        # Get some statistics on the current epoch (loss and gradient norm)
        pass
    
    @abstractmethod
    def solve_surrogate_problem(self, k, X, y):
        # Solve the surrogate problem for the kth agent
        pass
    
    def solve_surrogate_problem_parallel(self, k, X, y):
        # Solve surrogate problem for the kth agent in a parallel fashion
        return self.solve_surrogate_problem(k, X, y), 0
    
class PL_Ridge_NEXT(NEXT):
    # Ridge regression cost with PL strategy (see Sec. 4.2 in the paper)

    def initialize(self, X, y, kf, C, net, isClassification):
        ac.LasagneUpdate.initialize(self, X, y, kf, C, net, isClassification)
        self.compute_error_grad_fcn = theano.function([net.input_var, net.target_var], T.grad(self.error_loss, net.params))
        NEXT.initialize(self, X, y, kf, C, net, isClassification)
        if self.processors > 1:
            # Compute the partition of the weights across the processors
            self.theta_partition = cross_validation.KFold(self.N_params, self.processors)
        
    def compute_error_grad(self, X, y, tst_idx):
        return utils.vectorize(self.compute_error_grad_fcn(X[tst_idx,:], y[tst_idx]))
    
    def get_statistics(self, X, y, C, net):
        l = self.compute_loss_fcn(X, y)
        g = np.linalg.norm(self.compute_grad_fcn(X, y))**2
        return l, g
        
    def solve_surrogate_problem(self, k, X, y, tst_idx, net):
        # Build A and b matrices (using functions from class PL_SCA)
        Ak, bk = self.build_Ab_matrices(net, X[tst_idx, :], y[tst_idx], self.theta_curr[k,:])
        
        # Add the term corresponding to the pi variables
        bk -= 0.5*self.pi_curr[k,:].reshape(self.N_params, 1)
            
        # Get solution of surrogate problem
        start = timer()
        theta_hat = np.linalg.solve(Ak, bk)
        return theta_hat, timer()-start
        
    def solve_surrogate_problem_parallel(self, k, X, y, tst_idx, net):
            
            # Build Jacobian matrix and residual vector
            start = timer()
            J = self.build_jacobian_matrix(net, X[tst_idx])
            r = y[tst_idx] - net.get_output(X[tst_idx]) + np.dot(J, self.theta_curr[k,:].T).reshape([tst_idx.shape[0], 1])
            el_time = timer() - start          
            
            # Solve surrogate subproblems
            start = timer()
            theta_hat = np.zeros(self.N_params)
            for (tr_idx, k_idx) in self.theta_partition:
                
                # Compute local values
                A_rowblock = np.dot(J[:, k_idx].T, J)
                Ak = A_rowblock[:, k_idx] + (self.lambda_l2+self.tau)*0.5*np.eye(k_idx.shape[0])
                bk = np.dot(J[:, k_idx].T, r) + 0.5*self.tau*self.theta_curr[k, k_idx].reshape(k_idx.shape[0], 1) - \
                    0.5*self.pi_curr[k,k_idx].reshape(k_idx.shape[0], 1) - np.dot(A_rowblock[:, tr_idx], self.theta_curr[k, tr_idx]).reshape(k_idx.shape[0], 1)

                # Solve local problem                
                theta_hat[k_idx] = np.linalg.solve(Ak, bk)
                
            return theta_hat, el_time+(timer()-start)/self.processors

class PL_CrossEntropy_NEXT(PL_Ridge_NEXT):
    # Cross entropy cost with PL strategy (see Sec. 6.4 in the paper)

    def initialize(self, X, y, kf, C, net, isClassification):
        PL_Ridge_NEXT.initialize(self, X, y, kf, C, net, isClassification)
        
        # Redefine the Jacobian symbolic function for the linear part
        self.jac = theano.function([net.input_var], T.grad(T.sum(net.output_lin), net.params))
        self.net_jacobian = lambda x: np.concatenate([J.flatten() for J in self.jac(x)])
    
        self.sigmoid = lambda s: 1/(1+np.exp(-s))

    def compute_obj_and_grad(self, theta, J, yk, y_net, thetak, pik, net):
        
        tmp = self.sigmoid(y_net + np.dot(J, theta - thetak).reshape(yk.shape[0], 1))
        
        l = - np.sum(yk*np.log(tmp) + (1-yk)*np.log(1-tmp)) + \
            (self.lambda_l2 + self.tau)*0.5*np.dot(theta.T, theta) + \
            np.dot((pik - self.tau*thetak.T), theta)
            
        g = - np.sum(yk*(tmp*(1-tmp)*J/(tmp)) - (1-yk)*(tmp*(1-tmp)*J)/(1-tmp)) + \
            (self.lambda_l2 + self.tau)*theta + pik.T - \
            self.tau*thetak

        return l, g            
    
    def solve_surrogate_problem(self, k, X, y, tst_idx, net):
        
        J = self.build_jacobian_matrix(net, X[tst_idx])
        y_net = net.get_output_lin(X[tst_idx])

        f = lambda theta, *args: self.compute_obj_and_grad(theta, J, y[tst_idx], y_net, self.theta_curr[k], self.pi_curr[k], net)
        
        res = scipy.optimize.minimize(f, self.theta_curr[k], method='L-BFGS-B', 
                                jac=True, args={'maxiter': 250})
                                
        return res.x.astype(np.float32, copy=False), 0
        
class FL_Ridge_NEXT(PL_Ridge_NEXT):
    # Ridge regression cost with FL strategy (see Sec. 4.2 in the paper)    
    
    def solve_surrogate_problem(self, k, X, y, tst_idx, net):
        g = self.compute_error_grad(X, y, tst_idx)        
        return (1/(self.tau + self.lambda_l2))*(self.tau*self.theta_curr[k] - g - self.pi_curr[k]), 0