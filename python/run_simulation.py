from sklearn import datasets, preprocessing, cross_validation, metrics
import numpy as np
import theano
import utils, algorithms_centralized as ac, algorithms_distributed as ad, wrappers as w
import pandas
import pylab as pl, brewer2mpl
import networkx as nx
import lasagne.updates

"""
This code compares a set of centralized and/or distributed algorithms for training
a neural network model. The neural network is implemented based on the Lasagne
and Theano frameworks, using a wrapper class defined in the 'utils' module.

All aspects of the simulation, including the algorithms and the neural network
topology can be customized accordingly by varying the settings below.

The simulation setup and the distributed algorithms based on the NEXT framework
are discussed in the following preprint paper:

    S. Scardapane and P. Di Lorenzo (2016). "A Framework for Parallel and Distributed 
    Training of Neural Networks". arXiv preprint arxiv/1610.07448.
    https://arxiv.org/abs/1610.07448
    
Algorithms are currently organized in the following taxonomy.
    
    LasagneUpdate - Interface to Lasagne updates. The cost function and gradient functions 
          |         defined here are shared with most of the other algorithms.
          |
          |-- ScipyMinimize - Interface to SciPy.optimize.
          |
          |-- PL_SCA - Centralized version of PL-NEXT (see Eqs. (41)-(42) in the paper).
                 |
                 |-- NEXT - Template class for distributed algorithms based on NEXT.
                       |
                       |-- PL_Ridge_NEXT - PL version with ridge regression cost (Sec. 4.1 in the paper).
                                 |
                                 |-- PL_CrossEntropy_NEXT - PL version with cross-entropy cost.
                                 |
                                 |-- FL_Ridge_NEXT - FL version with ridge regression cost.
          |
          |-- DistGrad - Distributed gradient algorithm (derives from LasagneUpdate).

This code is still under active development, since different parts are not as efficient
as they could be, and new functionalities may be implemented at any time.

Author: Simone Scardapane (simone.scardapane@uniroma1.it)
"""

# Set random seed for reproducibility
import numpy.random
numpy.random.seed(1)

###########################
### PARAMETER SELECTION ###
###########################

# Number of epochs for training the models
max_epochs = 1000

# Number of repetitions for the experiments
runs = 1

# Number of agents in the network
I = 10

# Connectivity between agents in the network
p = 0.4

# Hidden nodes for the neural network
hidden_layers = np.array([10])

# List of algorithms to be tested (see paper for explanations)
lambda_l2 = 10**-1 # Regularization parameter

algo = {'Dummy': ac.SklearnDummyRegressor(), 
        'SGD': ac.LasagneUpdate(lambda_l2=lambda_l2, update_fcn=lasagne.updates.sgd, l_rate=0.001),
        'RmsProp': ac.LasagneUpdate(lambda_l2=lambda_l2, update_fcn=lasagne.updates.rmsprop, l_rate=0.001),
        'AdaGrad': ac.LasagneUpdate(lambda_l2=lambda_l2, update_fcn=lasagne.updates.adagrad, l_rate=0.1),
        'PL-SCA': ac.PL_SCA(lambda_l2=lambda_l2, l_rate=0.1, l_rate_epsilon=0.01, tau=0),
        'CG': ac.ScipyMinimize(lambda_l2=lambda_l2),
        'BFGS': ac.ScipyMinimize(minimize_fcn='L-BFGS-B', lambda_l2=lambda_l2),
        'DistGrad': ad.DistGrad(lambda_l2=lambda_l2, l_rate=0.003, l_rate_epsilon=0.01),
        'PL-NEXT': ad.PL_Ridge_NEXT(lambda_l2=lambda_l2, l_rate=0.05, l_rate_epsilon=0.01, tau=1),
        'FL-NEXT': ad.FL_Ridge_NEXT(lambda_l2=lambda_l2, l_rate=0.001, l_rate_epsilon=0.01, tau=1)
}

# Wrapper section (under development). Wrappers can be used to fine-tune the parameters.
# Remember to install the package "widgetsnbextension" for the progress bar
#r = numpy.logspace(-1, -10, num=5, base=2.0)
#algo['DistGrad'] = w.MinimizeSumOfErrors(algo['DistGrad'], {'l_rate': r, 'l_rate_epsilon': r})
#algo['PL-NEXT'] = w.MinimizeSumOfErrors(algo['PL-NEXT'], {'l_rate': r, 'tau': r})
#algo['FL-NEXT'] = w.MinimizeSumOfErrors(algo['FL-NEXT'], {'l_rate': r, 'tau': r})

###########################
### MAIN SIMULATION #######
###########################

# Used to scale the dataset
scaler = preprocessing.MinMaxScaler()

# Set to true if you are working on a classification dataset (to enable cross-entropy losses)
isClassification = False

# Dataset loading (BOSTON)
d = datasets.load_boston()
X = scaler.fit_transform(d.data).astype(theano.config.floatX)
y = scaler.fit_transform(d.target.reshape(-1, 1)).astype(theano.config.floatX)

# Generate connectivity
topology, C = utils.generate_random_topology(I, p)

# Output structures
N = len(algo.values())                      # Number of algorithms to be tested
err = np.zeros((N, runs))                   # Final test error
times_matrix = np.zeros((N, runs))          # Final training time
loss_matrix = np.zeros((N, max_epochs))     # Evolution of loss function
grad_matrix = np.zeros((N, max_epochs))     # Evolution of gradient norm
dis_matrix = np.zeros((N, max_epochs))      # Evolution of disagreement
err_matrix = np.zeros((N, max_epochs))      # Evolution of test error
comm_matrix = np.zeros((N, max_epochs))     # Number of scalars exchanged at every iteration
           
for r in range(runs):
    
    print('RUN ', r+1, ' OF ', runs, flush=True)    

    # Split the dataset for testing
    X_trn, X_tst, y_trn, y_tst = cross_validation.train_test_split(X, y, test_size=0.33)

    # Generate a generic neural network with lasagne
    net = utils.LasagneNet(X.shape[1], hidden_layers)
    
    # Split the training data over the agents
    if(I > 1):
        # We use a K-fold cross-validation to split the dataset evenly
        kf = cross_validation.KFold(X_trn.shape[0], n_folds=I)
    else:
        # Fake k-fold partition with k=1
        kf = list()
        kf.append((list(), np.arange(X_trn.shape[0])))

    # Finished optimization flags
    finished = [False] * N

    for a in algo.values():
        # Initialize the algorithms
        a.initialize(X_trn, y_trn, kf, C, net, isClassification)
    
    for a, a_idx in zip(algo.keys(), range(N)):  
        
        print('\tExecuting algorithm ', a, '...', flush=True)
        
        # Train the algorithm
        [iters, l, g, d, t, e, c] = algo[a].train(X_trn, y_trn, kf, C, net, max_epochs, X_tst, y_tst, isClassification)        
        
        # Update output structures
        loss_matrix[a_idx, 0:iters] += l
        grad_matrix[a_idx, 0:iters] += g
        dis_matrix[a_idx, 0:iters] += d
        err_matrix[a_idx, 0:iters] += e
        comm_matrix[a_idx, 0:iters] += c
        
        if iters > 0:
            # If optimization finished before 'max_epochs', set the remaining values
            # to the last obtained value.
            loss_matrix[a_idx, iters:] += l[iters-1]    
            grad_matrix[a_idx, iters:] += g[iters-1]
            dis_matrix[a_idx, iters:] += d[iters-1]
            err_matrix[a_idx, iters:] += e[iters-1]
            
        times_matrix[a_idx, r] += t        
        
        # Test the algorithm
        if isClassification:
            err[a_idx, r] = 1 - metrics.accuracy_score(y_tst, np.round(algo[a].predict(X_tst, net)))
        else:
            err[a_idx, r] = metrics.mean_squared_error(y_tst, algo[a].predict(X_tst, net))
        
        # Reset to the original parameters
        net.reset_parameters()
    
# Show results
pandas.set_option('display.width', 200)
print(pandas.DataFrame(np.vstack((err.mean(axis=1), err.std(axis=1), times_matrix.mean(axis=1), times_matrix.std(axis=1))).T, 
                       algo.keys(), ['MSE', 'MSE (std.)', 'Tr. time', 'Tr. time (std.)']).head(10))

# Show agents' network
pl.figure(0)
pl.title('Network topology')
pl.clf()
nx.draw(topology)
pl.show()

# Make plots
 # Get a colors matrix
if len(algo) > 3:
    bmap = brewer2mpl.get_map('Set3', 'qualitative', len(algo))
else:
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 3)
colors = bmap.mpl_colors
names = algo.keys()

# Evolution of the objective function
pl.figure(1)
pl.clf()
pl.xlabel('Epoch')
pl.ylabel('Objective function')
pl.yscale('log')
pl.title('Objective function')
for i,k in zip(range(N), algo.keys()):
    if not np.sum(np.abs(loss_matrix[i,:])) == 0:
        pl.plot(range(max_epochs), loss_matrix[i,:]/runs, label=k, color=colors[i]) 
        leg = pl.legend(loc='upper right')
pl.grid()

# Evolution of the gradient norm
pl.figure(2)
pl.clf()
pl.xlabel('Epoch')
pl.ylabel('Gradient norm')
pl.yscale('log')
pl.title('Gradient norm')
for i,k in zip(range(N), algo.keys()):
    if not np.sum(np.abs(grad_matrix[i,:])) == 0:
        pl.plot(range(max_epochs), grad_matrix[i,:]/runs, label=k, color=colors[i]) 
        leg = pl.legend(loc='upper right')
pl.grid()

# Evolution of the disagreement
pl.figure(3)
pl.clf()
pl.xlabel('Epoch')
pl.ylabel('Disagreement')
pl.yscale('log')
pl.title('Disagreement')
for i,k in zip(range(N), algo.keys()):
    if not np.sum(np.abs(dis_matrix[i,:])) == 0:
        pl.plot(range(1, max_epochs), dis_matrix[i,1:]/runs, label=k, color=colors[i]) 
        leg = pl.legend(loc='upper right')
pl.grid()

# Evolution of disagreement per scalars exchanged
pl.figure(4)
pl.clf()
pl.xlabel('Scalars exchanged')
pl.ylabel('Disagreement')
pl.yscale('log')
pl.title('Disagreement')
for i,k in zip(range(N), algo.keys()):
    if not np.sum(np.abs(dis_matrix[i,:])) == 0:
        pl.plot(np.cumsum(comm_matrix[i,0:-1]/runs), (dis_matrix[i,1:]/runs), label=k, color=colors[i]) 
        leg = pl.legend(loc='upper right')
pl.grid()

# Evolution of the test error
pl.figure(5)
pl.clf()
pl.xlabel('Epoch')
pl.ylabel('Test error')
pl.yscale('log')
pl.title('Test error')
for i,k in zip(range(N), algo.keys()):
    if not np.sum(np.abs(err_matrix[i,:])) == 0:
        pl.plot(range(max_epochs), err_matrix[i,:]/runs, label=k, color=colors[i]) 
        leg = pl.legend(loc='upper right')
pl.grid()

# Evolution of the test error per scalars exchanged
pl.figure(6)
pl.clf()
pl.xlabel('Scalars exchanged')
pl.ylabel('Test error')
pl.yscale('log')
pl.title('Test error')
for i,k in zip(range(N), algo.keys()):
    if not np.sum(np.abs(comm_matrix[i,0:-1])) == 0:
        c = np.insert(np.cumsum(comm_matrix[i,0:-1]/runs), 0, 0)
        pl.plot(c, (err_matrix[i,:]/runs), label=k, color=colors[i]) 
        leg = pl.legend(loc='upper right')
pl.grid()