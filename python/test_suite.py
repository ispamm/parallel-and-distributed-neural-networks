# -*- coding: utf-8 -*-

import unittest
import numpy as np
import utils
import algorithms_distributed as ad
from sklearn import cross_validation

class TestPL_Next(unittest.TestCase):

    def test_first_iteration(self):

        np.random.seed(100)        
        
        # Define a fake dataset
        X = np.asarray([[0.1, 0.2],[0.3, 0.4],[0.5, 0.6]])
        y = np.asarray([1, -2, 3]).reshape(3, 1)
        
        # Define the data split
        kf = cross_validation.KFold(3, n_folds=3)        
        
        # Define the neural network
        net = utils.LasagneNet(2, [2])
        
        # Define the topology
        t, C = utils.generate_random_topology(3, 0.1)
        
        # Initialize the algorithm
        a = ad.PL_Ridge_NEXT(lambda_l2=0.1)
        a.initialize(X, y, kf, C, net)

        # Compute initial v and pi
        thetaCurr = a.theta_curr
        v = np.zeros((9, 3))
        pi = np.zeros((9, 3))
        for k,(tr_idx,tst_idx) in zip(np.arange(C.shape[0]), kf):
            net.set_parameters(thetaCurr[k])
            g = a.compute_error_grad(X, y, tst_idx)
            v[:, k] = g
            pi[:, k] = 3*v[:, k] - v[:, k]
        
        np.testing.assert_array_almost_equal(v, a.v_curr.T, decimal=8)
        np.testing.assert_array_almost_equal(pi, a.pi_curr.T, decimal=8)
        
        # Run the algorithm for one epoch
        a.train(X, y, kf, C, net, 1, np.random.random((1, 2)), np.random.random((1, 1)))        
        
        # Solve local optimization problems
        thetaNew = np.zeros((9, 3))
        for k,(tr_idx,tst_idx) in zip(np.arange(C.shape[0]), kf):
            net.set_parameters(thetaCurr[k])
            Ak, bk = a.build_Ab_matrices(net, X[tst_idx], y[tst_idx], thetaCurr[k], 3)
            bk = bk - 0.5*pi[:, k].reshape(9,1)
            thetaNew[:, k] = np.linalg.pinv(Ak).dot(bk).T
            thetaNew[:, k] = thetaCurr[k] + a.l_rate*(thetaNew[:,k].T - thetaCurr[k])

        # Perform first consensus step
        thetaCurr = np.dot(thetaNew, C).T
        
        # Update v and pi variables
        vNew = np.empty_like(v)
        vNew[:] = v
        for k,(tr_idx,tst_idx) in zip(np.arange(C.shape[0]), kf):
            net.set_parameters(thetaCurr[k])
            g = a.compute_error_grad(X, y, tst_idx)
            vNew[:, k] = np.dot(v, C[:,k]) + (g - v[:, k])
            pi[:, k] = 3*vNew[:, k] - g
        v[:] = vNew
        
        # Check everything
        np.testing.assert_array_almost_equal(thetaCurr, a.theta_curr, decimal=3)
        np.testing.assert_array_almost_equal(v, a.v_curr.T, decimal=3)
        np.testing.assert_array_almost_equal(pi, a.pi_curr.T, decimal=3)
        
if __name__ == '__main__':
    unittest.main()