# -*- coding: utf-8 -*-
"""
A small set of utility functions
"""

import networkx as nx
import numpy as np
import lasagne
import theano
import theano.tensor as T

def generate_random_topology(I, p):
    # Create a random network topology with I agents and connectivity p.
    # Return a tuple with the topology (a network object) and the mixing 
    # weights computed according to the Metropolis rule.
    
    # Iterate until a connected topology is found
    while True:
        topology = nx.gnp_random_graph(I, p)
        if(nx.is_connected(topology)):
            break
    adjacency = nx.adjacency_matrix(topology).todense()
    
    # Compute the mixing weights (Metropolis)
    C = np.zeros((I,I), dtype=theano.config.floatX)
    degrees = adjacency.sum(axis=0).T
    for k1 in np.arange(I):
        for k2 in np.arange(I):
            if adjacency[k1,k2] == 1:
                C[k1,k2] = 1/(np.max([degrees[k1], degrees[k2]]) + 1)
        C[k1,k1] = 1 - C[k1,:].sum(axis=0)
    
    # Return the values
    return topology, C
    
def vectorize(parameters):
    # Utility function for vectorizing a list of matrices into a single vector
    return np.hstack([np.ravel(a) for a in parameters])
    
def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])
           
    See: http://stackoverflow.com/questions/1208118/using-numpy-to-build-an-array-of-all-combinations-of-two-arrays
    """

    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = int(n / arrays[0].size)
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in range(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out    
    
class LasagneNet:
    # Simple wrapper class for a feedforward Lasagne network
    def __init__(self, d, hidden_layers):
        # Input and output variables
        self.input_var = T.matrix('input')
        self.target_var = T.matrix('target')
        
        # Build the actual network
        l_in = lasagne.layers.InputLayer(shape=(None, d), input_var=self.input_var)                           
        l_hid = np.empty(len(hidden_layers), dtype=object)
        l_hid[0] = lasagne.layers.DenseLayer(
            l_in, num_units=hidden_layers[0],
            nonlinearity=lasagne.nonlinearities.tanh)
        for i,hidden in zip(np.arange(1,len(hidden_layers)), hidden_layers[1:]):
            l_hid[i] = lasagne.layers.DenseLayer(
                l_hid[i-1], num_units=hidden,
                nonlinearity=lasagne.nonlinearities.tanh)
        self.lasagne_net = lasagne.layers.DenseLayer(
            l_hid[len(l_hid)-1], num_units=1,
            # We need the linear part of the last layer for the cross-entropy formulation
            # See Eq. (33) in the paper
            nonlinearity=lasagne.nonlinearities.identity)
            
        # Get the linear output
        self.output_lin = lasagne.layers.get_output(self.lasagne_net)
        
        # Add the final nonlinearity
        self.output = lasagne.nonlinearities.sigmoid(lasagne.layers.get_output(self.lasagne_net))
        
        # Get all parameters
        self.params = lasagne.layers.get_all_params(self.lasagne_net, trainable=True)
        
        # Get the initial parameters
        self.start_params = lasagne.layers.get_all_param_values(self.lasagne_net)
    
        # Get linear and nonlinear output
        self.get_output_lin = theano.function([self.input_var], self.output_lin)
        self.get_output = theano.function([self.input_var], self.output)
        
    def reset_parameters(self):
        # Utility function for resetting parameters
        lasagne.layers.set_all_param_values(self.lasagne_net, self.start_params)
        return self
        
    def set_parameters(self, theta_new):
        # Set the parameters of a Lasagne network starting from a vector
        def inplaceupdate(x, new):
            x[...] = new
            return x

        paramscounter = 0
        for p in self.params:
            pshape = p.get_value().shape
            pnum = np.prod(pshape)
            p.set_value(inplaceupdate(p.get_value(borrow=True), theta_new[paramscounter:paramscounter+pnum].reshape(*pshape)), borrow=True)
            paramscounter += pnum 
        
    def get_params_vector(self):
        # Get the parameters of the network as a vector
        p = T.vector()
        #p = T.specify_shape(p, [vectorize(self.start_params).shape[0]])
        paramscounter = 0
        for par in self.params:
            pshape = par.get_value().shape
            for r in range(pshape[0]):
                if len(pshape) == 2:
                    hor_dim = pshape[1]
                else:
                    hor_dim = 1
                T.set_subtensor(p[paramscounter:paramscounter+hor_dim], par[r])
                paramscounter += hor_dim
        return p