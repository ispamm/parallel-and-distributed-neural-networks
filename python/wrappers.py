# -*- coding: utf-8 -*-
"""
Created on Thu Jul 28 15:57:40 2016

@author: ISPAMM
"""

import utils
import numpy as np
from tqdm import trange

class MinimizeSumOfErrors:
    
    def __init__(self, wrapped_algo, params):
        self.wrapped_algo = wrapped_algo
        self.params = params
        
    def initialize(self, X, y, kf, C, net):
        return
        
    def train(self, X, y, kf, C, net, max_epochs, X_tst, y_tst):
        
        comb = utils.cartesian(self.params.values())
        err_min = np.inf
        p_best = None

        p_names = self.params.keys()        
        
        print("\tEvaluating ", comb.shape[0], " combinations of parameters...", flush=True)
        
        for k in trange(comb.shape[0], desc='\tProgress', unit="combination", ncols=70):
            p_curr = comb[k, :]
            net.reset_parameters()
            for i, name in zip(range(p_curr.shape[0]), p_names):
                setattr(self.wrapped_algo, name, p_curr[i])
            self.wrapped_algo.initialize(X, y, kf, C, net)
            err = self.wrapped_algo.train(X, y, kf, C, net, max_epochs, X_tst, y_tst)
            if np.sum(err[1]) < err_min:
                err_min = np.sum(err[1])
                p_best = p_curr
        
        for i, name in zip(range(p_best.shape[0]), p_names):
                setattr(self.wrapped_algo, name, p_best[i])
           
        print("\n\tBest parameters:", flush=True)
        for (n, p) in zip(self.params.keys(), range(p_best.shape[0])):
            print("\t\t", n, ": ", p_best[p])
        print("\tFinal training...", flush=True)
        net.reset_parameters()
        self.wrapped_algo.initialize(X, y, kf, C, net)
        return self.wrapped_algo.train(X, y, kf, C, net, max_epochs, X_tst, y_tst)
        
    def predict(self, X, net):
        return self.wrapped_algo.predict(X, net)
    