% TEST_SCRIPT - Run the overall simulation.
%   Parameters are specified in the 'params_selection.m' script.

% License to use and modify this code is granted freely without warranty to all, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.
%
% Programmed and Copyright by Simone Scardapane:
% simone.scardapane@uniroma1.it

% Reset the workspace
clear all; 
close all;
clc; 
rng(1); % Set random seed
f = genpath(pwd); addpath(f);

% Select parameters for the simulation
params_selection;

% Load dataset and normalize
load(datasetFile);
dataset = normalize(d, 0, 1);

% Select proper error function and adjust output values
if(dataset.task == Tasks.R)
    % Error function (MSE)
    errFcn = @(d,y) mean((d - y).^2);
    dataset.Y = mapminmax(dataset.Y',0, 1);
    dataset.Y = dataset.Y';
elseif(dataset.task == Tasks.BC)
    % Error function (CLASSIFICATION ERROR, BINARY)
    errFcn = @(d,y) sum(round(d) ~= round(y))/length(d);
    dataset.Y(dataset.Y == 1) = 0.05;
    dataset.Y(dataset.Y == 0) = 0.95;
    dataset.Y(dataset.Y == -1) = 0.95;
end

% Split the dataset
dataset = dataset.generateNPartitions(runs, KFoldPartition(kfolds));

% -----------------------------
% --- MAIN SIMULATION ---------
% -----------------------------

% Initialize output matrices
N_algorithms = length(centralized_algorithms) + length(distributed_algorithms)*length(agents);
errors = zeros(runs*kfolds, N_algorithms);
times = zeros(runs*kfolds, N_algorithms);
sparsity = zeros(runs*kfolds, N_algorithms);

% Structures for saving internal evaluations of objective function and
% gradient norm
trainInfo = cell(runs*kfolds, N_algorithms);

% Initialize the networks' topologies
nets = cell(length(agents), 1);
for ii = 1:length(agents)
	nets{ii} = RandomTopology(agents(ii), 'metropolis', connectivity);
end

z = 1;  % Auxiliary index for the output structures

for n = 1:runs
    
    fprintf('--- RUN %i/%i ---\n', n, runs);
    
    % Set the current partition in the dataset
    dataset = dataset.setCurrentPartition(n);
    
    % Initialize all algorithms with the same weights
    W_in_h = randn(size(dataset.X, 2) + 1, hidden)*0.01;
    W_h_out = randn(hidden + 1, size(dataset.Y, 2))*0.01;
    centralized_algorithms_current = cellfun(@(a) init_algorithm(a, d, W_in_h, W_h_out), centralized_algorithms, 'UniformOutput', false);
    distributed_algorithms_current = cellfun(@(a) init_algorithm(a, d, W_in_h, W_h_out), distributed_algorithms, 'UniformOutput', false);
    
    for k = 1:kfolds
       
        % Split and distribute the data
        [trainData, testData] = dataset.getFold(k);
        trainDataDistributed = cell(length(agents), 1);
        for ii = 1:length(agents)
            if(agents(ii) > 1)
                distrPartition = KFoldPartition(agents(ii));
            else
                distrPartition = NoPartition();
            end
            trainDataDistributed{ii} = trainData.distributeDataset(distrPartition);
        end
        
        fprintf('\tFold %i/%i - %i training, %i test\n', k, kfolds, size(trainData.X, 1), size(testData.X, 1));
        
        % Test the centralized algorithms
        for a = 1:length(centralized_algorithms)
            fprintf('\t\tTraining %s...\n', centralized_algorithms{a}.name);
            [tmp, trainInfo{z,a}] = ...
                centralized_algorithms_current{a}.train(trainData);
            times(z, a) = trainInfo{z,a}.training_time;
            errors(z, a) = errFcn(testData.Y, tmp.test(testData));
            if(isa(centralized_algorithms_current{a}, 'MultilayerPerceptron'))
                sparsity(z, a) = sum(abs(tmp.getWeights()) < 10^-25)/length(tmp.getWeights());
            end
        end
        % Correct for the index
        if isempty(centralized_algorithms)
            a = 0;
        end
        a = a + 1;
        
        % Test the distributed algorithms
        for b = 1:length(distributed_algorithms)
            for ii = 1:length(agents)
                L = agents(ii);
                fprintf('\t\tTraining %s (%i agents)...\n', distributed_algorithms{b}.name, L);
                [tmp, trainInfo{z,a}] = ...
                    distributed_algorithms_current{b}.train(trainDataDistributed{ii}, nets{ii});
                times(z, a) = trainInfo{z,a}.training_time;
                errors(z, a) = errFcn(testData.Y, tmp.test(testData));
                if(isa(distributed_algorithms_current{b}, 'MultilayerPerceptron'))
                    sparsity(z, a) = sum(abs([tmp.W_in_h(:); tmp.W_h_out(:)]) < 10^-25)/length([tmp.W_in_h(:); tmp.W_h_out(:)]);
                end
                a = a + 1;
            end
        end
        
        z = z + 1;
    end
    
end

%% -----------------------------
% --- OUTPUT-------------------
% -----------------------------

fprintf('-----------------------\n');
fprintf('--- RESULTS -----------\n');
fprintf('-----------------------\n');

% First, we associate with each agents' configuration a unique string, e.g.
% if we have agents = [1, 1, 3] we obtain [1a, 1b, 3], in order to
% distinguish them in the plots.
agents_unique = unique(agents);
agents_name = cell(length(agents), 1);
for i = 1:length(agents_unique)
    tmp = agents == agents_unique(i);
    if(sum(tmp) > 1)
        agents_name(tmp) = cellstr(strcat(num2str(agents_unique(i)),  char(double('a'):double('a')+sum(tmp)-1)'));
    else
        agents_name{tmp} = num2str(agents_unique(i));
    end
end


% Collect the names of the algorithms (in the output, there is one
% algorithm for each tested size of the network)
names = cell(N_algorithms, 1);
i = [];
for i=1:length(centralized_algorithms)
    names{i} = centralized_algorithms{i}.name;
end
if(isempty(centralized_algorithms))
    i = 1;
else
    i = i + 1;
end
for j=1:length(distributed_algorithms)
    for z=1:length(agents)
        names{i} = [distributed_algorithms{j}.name, ' (', agents_name{z}, ')'];
        i = i + 1;
    end
end

% Show the output tables and plots
disptable([mean(errors,1)' std(errors,1)' mean(times,1)' std(times,1)' mean(sparsity,1)'], {'Tst. error', 'Tst. error (std)', 'Tr. times', 'Tr. times (std)', 'Sparsity'}, names); 
make_plots;