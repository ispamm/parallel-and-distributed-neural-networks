classdef AdditionalTests < matlab.unittest.TestCase

    
    methods (Test)

        function testMatrixReconstruction(testCase)
            % Test vectorization and reconstruction
            
            % Generate weight matrices
            Win = rand(3, 2);
            Wout = rand(3, 1);
            
            % Vectorize and concatenate
            W = [Win(:); Wout(:);];
            
            % Reshape
            Win_new = reshape(W(1:numel(Win)), 3, 2);
            Wout_new = reshape(W(numel(Win)+1:end), 3, 1);
            
            % Test validity of results
            testCase.assertEqual(Win, Win_new);
            testCase.assertEqual(Wout, Wout_new);

        end
        
        function testMatrixStacking(testCase)
            % Test stacking of 3D matrix over rows
            
            A = rand(3, 2, 2);
            Apermuted = permute(A, [1 3 2]);
            
            Areshaped = reshape(Apermuted, [], 2);
            
            % Test result
            expectedResult = [A(:,:,1); A(:,:,2)];
            testCase.assertEqual(Areshaped, expectedResult);

        end
        
        function test3DProductByScalar(testCase)
            % Test multiplication of matrix A by vector b such that the
            % result is a 3D matrix where the (:,:,i)th slice is the
            % multiplication A*b(i)
            
            A = rand(3, 2);
            b = rand(2, 1);
            
            Atimes = bsxfun(@times, permute(A, [3 1 2]), b);
            Apermuted = permute(Atimes, [2 3 1]);
            
            expectedResult = cat(3, A*b(1), A*b(2));
            
            testCase.assertEqual(Apermuted, expectedResult);
            
        end
    end  
    
end

