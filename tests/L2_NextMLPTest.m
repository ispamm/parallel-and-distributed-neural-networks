classdef L2_NextMLPTest < matlab.unittest.TestCase

    
    methods (Test)

        % Test first iteration of the algorithm on a toy problem
        function testFirstIteration(testCase)

            % Define network and dataset
            n = L2_NextMLP('', 2, 'epochs', 1, 'lambda', 0.1, 'tau', 0, 'matlab_cost', true);
            dataset = Dataset('', [.1 .2; .3 .4; .5 .6], [1; -2; 3], Tasks.R);
            dataset = dataset.distributeDataset(KFoldPartition(3));
            n = n.init(dataset);
            
            % Define the network's topology
            net = LinearTopology(3, 'metropolis', 1);
            
            % Take seed
            s = rng();
            
            % Get the initial weights of the network
            thetaCurr = zeros(9, 3);
            for k = 1:3
                tmp = n.init(dataset);
                thetaCurr(:, k) = [tmp.W_in_h(:); tmp.W_h_out(:)];
            end

            % Reset the prng
            rng(s);
            
            % Initialize v and pi
            n = n.initSCA(dataset, net);
            n.thetaCurr = thetaCurr;
            v = zeros(9, 3);
            pi = zeros(9, 3);
            for k = 1:3
                n = n.reshapeWeights(thetaCurr(:, k));
                g = n.computeLossGradients(dataset.getLocalPart(k));
                v(:, k) = g;
                pi(:, k) = 3*v(:, k) - v(:, k);
            end
            
            c_l = ((1-n.lambda)/3);
            c_r = (n.lambda/9);
            
            % Store initial function and gradient norm
            n = n.reshapeWeights(mean(thetaCurr, 2));
            [pre, post] = n.forwardPass(dataset.X);
            [g_l, g_r] = n.backwardPass(pre, post, dataset.X, dataset.Y);
            fval_0 = c_l*sum((post{2} - dataset.Y).^2) +  c_r*sum(mean(thetaCurr, 2).^2);
            gval_0 = norm(c_l*[g_l{1}(:); g_l{2}(:)] + c_r*[g_r{1}(:); g_r{2}(:)], inf);
            
            % Solve local optimization problems
            thetaNew = zeros(9, 3);
            for k = 1:3
                dataset_local = dataset.getLocalPart(k);
                n = n.reshapeWeights(thetaCurr(:, k));
                [pre, post] = n.forwardPass(dataset_local.X);
                [A, b] = n.compute_AB(dataset_local.X, dataset_local.Y, pre, post, thetaCurr(:, k), 1, pi(:, k));
                thetaNew(:, k) = A\b;
                thetaNew(:, k) = thetaCurr(:, k) + (n.alpha0*(1-n.epsilon*n.alpha0))*(thetaNew(:,k) - thetaCurr(:,k));
            end
            
            % Consensus step on theta
            thetaCurr = thetaNew*net.W;
            n.thetaCurr = thetaCurr;
            
            % Compute new objective value and gradients norm
            n = n.reshapeWeights(mean(thetaCurr, 2));
            [pre, post] = n.forwardPass(dataset.X);
            [g_l, g_r] = n.backwardPass(pre, post, dataset.X, dataset.Y);
            fval_1 = c_l*sum((post{2} - dataset.Y).^2) +  c_r*sum(mean(thetaCurr, 2).^2);
            gval_1 = norm(c_l*[g_l{1}(:); g_l{2}(:)] + c_r*[g_r{1}(:); g_r{2}(:)], inf);
            
            % Update v and pi
            vNew = v;
            for k = 1:3
                n = n.reshapeWeights(thetaCurr(:, k));
                g = n.computeLossGradients(dataset.getLocalPart(k));
                vNew(:,k) = v*net.W(:,k) + (g - v(:, k));
                pi(:,k) = 3*vNew(:,k) - g;
            end
            v = vNew;
            
            % Train the network
            [n, trainInfo] = n.train(dataset, net);
            
            % Compute network's fval and gval
            n = n.reshapeWeights(mean(n.thetaCurr, 2));
            [pre, post] = n.forwardPass(dataset.X);
            [g_l, g_r] = n.backwardPass(pre, post, dataset.X, dataset.Y);
            fval_1_net = c_l*sum((post{2} - dataset.Y).^2) +  c_r*sum(mean(thetaCurr, 2).^2);
            gval_1_net = norm(c_l*[g_l{1}(:); g_l{2}(:)] + c_r*[g_r{1}(:); g_r{2}(:)], inf);
            
            % Check all the results
            
            % Test 1 - objective function values
            testCase.assertLessThan(abs(trainInfo.fval(1) - fval_0), 10^-6);
            testCase.assertLessThan(abs(fval_1 - fval_1_net), 10^-6);
            
            % Test 2 - gradient norms
            testCase.assertLessThan(abs(trainInfo.gval(1) - gval_0), 10^-6);
            testCase.assertLessThan(abs(gval_1 - gval_1_net), 10^-6);
            
            % Test 3 - final weightss
            testCase.assertLessThan(abs(n.thetaCurr - thetaCurr), 10^-6);
            
            % Test 4 - v variables
            testCase.assertLessThan(abs(n.vCurr - v), 10^-6);
            
            % Test 5 - pi variables
            testCase.assertLessThan(abs(n.piCurr - pi), 10^-6);
            
        end
        
    end  
    
end

