classdef L2_NextMLP_SingleAgent_Test < matlab.unittest.TestCase

    
    methods (Test)

        % Test evaluation of cost function and gradient norm
        function testFvalAndGval(testCase)
           
            % Take seed
            s = rng();
            
            % Define network and dataset
            n = L2_NextMLP('', 2, 'epochs', 1, 'lambda', 0.1, 'matlab_cost', true);
            dataset = Dataset('', [.1 .2; .3 .4], [1 2; -2 -1], Tasks.R);
            
            % Initialize the weights
            n = n.init(dataset);
            
            % Make forward and backward pass
            [pre, post] = n.forwardPass(dataset.X);
            [grad_l, grad_w] = n.backwardPass(pre, post, dataset.X, dataset.Y);
            
            % Reset seed
            rng(s);
            
            % Train the network
            dataset = dataset.distributeDataset(NoPartition());
            [~, trainInfo] = n.train(dataset, LinearTopology(1, 'metropolis', 1));
            
            % Compute manually cost function
            fval = (0.9/4)*((post{2}(1,1) - dataset.Y(1,1))^2 + ...
                (post{2}(1,2) - dataset.Y(1,2))^2 + ...
                (post{2}(2,1) - dataset.Y(2,1))^2 + ...
                (post{2}(2,2) - dataset.Y(2,2))^2) + ...
                (0.1/12)*norm([n.W_in_h(:); n.W_h_out(:)])^2;
            
            % Compute manually the gradient
            gval = norm((0.9/4)*[grad_l{1}(:); grad_l{2}(:)] + (0.1/12)*[grad_w{1}(:); grad_w{2}(:)], inf);
            
            % Check validity
            testCase.assertLessThan(abs(trainInfo.fval(1) - fval), 10^-6);
            testCase.assertLessThan(abs(trainInfo.gval(1) - gval), 10^-6);
            
            
        end
        
        % Test computation of A and b matrices (by hand)
        function testComputeAB(testCase)
           
            % Define network and dataset
            n = L2_NextMLP('', 2, 'epochs', 1, 'lambda', 0.1);
            d = Dataset('', [.1 .2; .3 .4], [1 2; -2 -1], Tasks.R);
           
            % Initialize the weights
            n = n.init(d);
            theta = [n.W_in_h(:); n.W_h_out(:)];
            
            % Make forward pass
            [pre, post] = n.forwardPass(d.X);
            
            % Compute Jacobian
            J = zeros(4, 12);
            
            for ii = 1:2    % Sample index
                for oo = 1:2    % Output index
                    
                    % Weights of hidden matrix
                    J((ii-1)*2 + oo, 1) = d.X(ii,1)*post{1}(ii,1)*(1-post{1}(ii,1))*n.W_h_out(1,oo)*post{2}(ii,oo)*(1-post{2}(ii,oo));
                    J((ii-1)*2 + oo, 2) = d.X(ii,2)*post{1}(ii,1)*(1-post{1}(ii,1))*n.W_h_out(1,oo)*post{2}(ii,oo)*(1-post{2}(ii,oo));
                    J((ii-1)*2 + oo, 3) = post{1}(ii,1)*(1-post{1}(ii,1))*n.W_h_out(1,oo)*post{2}(ii,oo)*(1-post{2}(ii,oo));
                    J((ii-1)*2 + oo, 4) = d.X(ii,1)*post{1}(ii,2)*(1-post{1}(ii,2))*n.W_h_out(2,oo)*post{2}(ii,oo)*(1-post{2}(ii,oo));
                    J((ii-1)*2 + oo, 5) = d.X(ii,2)*post{1}(ii,2)*(1-post{1}(ii,2))*n.W_h_out(2,oo)*post{2}(ii,oo)*(1-post{2}(ii,oo));
                    J((ii-1)*2 + oo, 6) = post{1}(ii,2)*(1-post{1}(ii,2))*n.W_h_out(2,oo)*post{2}(ii,oo)*(1-post{2}(ii,oo));
                
                    % Weights of output matrix
                    J((ii-1)*2 + oo, (oo-1)*3 + 7) = post{1}(ii,1)*post{2}(ii,oo)*(1-post{2}(ii,oo));
                    J((ii-1)*2 + oo, (oo-1)*3 + 8) = post{1}(ii,2)*post{2}(ii,oo)*(1-post{2}(ii,oo));
                    J((ii-1)*2 + oo, (oo-1)*3 + 9) = post{2}(ii,oo)*(1-post{2}(ii,oo));
                    
                end
            end
            
            % Compute r vectors
            r1 = d.Y(1,:)' - post{2}(1,:)' + J(1:2,:)*theta;
            r2 = d.Y(2,:)' - post{2}(2,:)' + J(3:4,:)*theta;
            
            % Compute A and b
            A = J(1:2,:)'*J(1:2,:) + J(3:4,:)'*J(3:4,:) + 0.1*eye(length(theta));
            b = (r1'*J(1:2,:) + r2'*J(3:4,:))';
            
            % Compute A and b from network
            n = n.initSCA(d, 1);
            [A2, b2, J2] = n.compute_AB(d.X, d.Y, pre, post, theta, 1, zeros(12, 1));
            
            % Check validity of results
            testCase.assertLessThan(abs(J2 - J), 10^-6);
            testCase.assertLessThan(abs(A2 - A), 10^-6);
            testCase.assertLessThan(abs(b2 - b), 10^-6);

        end
        
        % Test partitioning of A and b
        function testPartitionAB(testCase)
           
            % Define network and dataset
            n = L2_NextMLP('', 2, 'epochs', 1, 'lambda', 0.1, 'tau', 0.2);
            d = Dataset('', [.1 .2; .3 .4], [1 2; -2 -1], Tasks.R);
           
            % Initialize the weights
            n = n.init(d);
            theta = [n.W_in_h(:); n.W_h_out(:)];
            
            % Make forward pass
            [pre, post] = n.forwardPass(d.X);
            
            % Compute A and b from network
            n = n.startTimer();
            n = n.initSCA(d, LinearTopology(1, 'metropolis', 1));
            [A, b] = n.compute_AB(d.X, d.Y, pre, post, theta, 1, zeros(12, 1));
            
            % Declare symbolic partitioning
            theta_sym = sym('theta_sym', [12 1], 'real');
            
            % Loop for the number of processors going from 2 to 12
            for processors = 2:12

                % Partition the two matrices
                n.processors = processors;
                n_curr = n.initSCA(d, LinearTopology(1, 'metropolis', 1));
                [A_part, b_part] = n_curr.compute_AB(d.X, d.Y, pre, post, theta, processors, zeros(12, 1));
                
                % Check we update all weights only once
                idx_curr = zeros(12, 1);
                
                % Solve for each processor
                for i=1:processors
                    
                    % Declare symbolic variable
                    theta_sym_curr = theta_sym;
                    theta_sym_curr(n_curr.idx.training(i)) = theta(n_curr.idx.training(i));
                    
                    % Update counter
                    idx_curr(n_curr.idx.test(i)) = idx_curr(n_curr.idx.test(i)) + 1;
                    
                    % Declare cost function
                    costFunction = theta_sym_curr'*A*theta_sym_curr - 2*b'*theta_sym_curr;
                    
                    % Solve cost function
                    costFunction_grad = gradient(costFunction, theta_sym_curr(n_curr.idx.test(i)));
                    sol = vpasolve(costFunction_grad, theta_sym_curr(n_curr.idx.test(i)));
                    if(isa(sol,'sym') && length(sol) == 1)
                        sol = double(sol);
                    else
                        sol = struct2cell(sol);
                        sol = double(cat(2, sol{:}));
                    end
                    
                    % Solve with partitioned A and b
                    theta_hat = A_part{i}\b_part{i};
                    
                    % Check validity of solution
                    testCase.assertLessThan(abs(theta_hat - double(sol)'), 10^-6);
                end
                
                % Check counter
                testCase.assertEqual(idx_curr, ones(12, 1));
                
            end

        end

        % Test computation of the Jacobian (symbolic)
        function testJacobian(testCase)
           
            % Set seed
            rng(1);
            
            % Define network and dataset
            n = L2_NextMLP('', 2, 'epochs', 1, 'lambda', 0.1);
            d = Dataset('', [.1 .2; .3 .4], [1 2; -2 -1], Tasks.R);

            % Initialize the weights
            n = n.init(d);
            theta = [n.W_in_h(:); n.W_h_out(:)];
            
            % Make forward pass
            [pre, post] = n.forwardPass(d.X);
            
            % Compute Jacobian from network
            J = n.computeJacobian(pre, post, d.X);
            
            % Compute Jacobian in symbolic form
            
            % Define input and weights
            syms x1 x2 wi11 wi21 wi31 wi12 wi22 wi32 wo11 wo21 wo31 wo12 wo22 wo32 real;
            
            % syms sh1 sh2 h1 h2 so1 so2 y1 y2 real;
            syms sh h so y real;
            
            % Define hidden pre-activations
            sh1 = wi11*x1 + wi21*x2 + wi31;
            sh2 = wi12*x1 + wi22*x2 + wi32;
            
            % Define hidden activations
            h1 = 1/(1+exp(-sh1));
            h2 = 1/(1+exp(-sh2));
            
            % Define output pre-activations
            so1 = wo11*h1 + wo21*h2 + wo31;
            so2 = wo12*h1 + wo22*h2 + wo32;
            
            % Define output activations
            y1 = 1/(1+exp(-so1));
            y2 = 1/(1+exp(-so2));
            
            % Compute symbolic gradient
            g1 = gradient(y1, [wi11 wi21 wi31 wi12 wi22 wi32 wo11 wo21 wo31 wo12 wo22 wo32]);
            g2 = gradient(y2, [wi11 wi21 wi31 wi12 wi22 wi32 wo11 wo21 wo31 wo12 wo22 wo32]);
            
            % Evaluate gradient for input 1
            g1_input1 = subs(g1, [x1 x2 wi11 wi21 wi31 wi12 wi22 wi32 wo11 wo21 wo31 wo12 wo22 wo32], ...
                [d.X(1,1) d.X(1,2) theta']);
            g2_input1 = subs(g2, [x1 x2 wi11 wi21 wi31 wi12 wi22 wi32 wo11 wo21 wo31 wo12 wo22 wo32], ...
                [d.X(1,1) d.X(1,2) theta']);
            g1_input1 = vpa(g1_input1);
            g2_input1 = vpa(g2_input1);
            
            % Evaluate gradient for input 2
            g1_input2 = subs(g1, [x1 x2 wi11 wi21 wi31 wi12 wi22 wi32 wo11 wo21 wo31 wo12 wo22 wo32], ...
               [d.X(2,1) d.X(2,2) theta']);
            g2_input2 = subs(g2, [x1 x2 wi11 wi21 wi31 wi12 wi22 wi32 wo11 wo21 wo31 wo12 wo22 wo32], ...
               [d.X(2,1) d.X(2,2) theta']);
            g1_input2 = vpa(g1_input2);
            g2_input2 = vpa(g2_input2);
            
            % Check validity of results row-by-row
            testCase.assertLessThan(abs(J(1,:) - g1_input1'), 10^-1);
            testCase.assertLessThan(abs(J(2,:) - g2_input1'), 10^-1);
            testCase.assertLessThan(abs(J(3,:) - g1_input2'), 10^-1);
            testCase.assertLessThan(abs(J(4,:) - g2_input2'), 10^-1);
            
        end
        
        function testXOR(testCase)
            
            % Test using the XOR example taken from:
            % http://matlabgeeks.com/tips-tutorials/neural-networks-a-multilayer-perceptron-in-matlab/
            
            n = L2_NextMLP('', 2, 'epochs', 250, 'matlab_cost', false, 'lambda', 10^-15, 'tau', 0, 'threshold', 0, 'alpha0', 0.1, 'epsilon', 0.01);
            d = Dataset('', [0.1 0.1; 0.1 0.9; 0.9 0.1; 0.9 0.9], [0.1; 0.9; 0.9; 0.1], Tasks.R);

            % Initialize the network
            n = n.init(d);
            
            % Train the network
            d = d.distributeDataset(NoPartition());
            [n, trainInfo] = n.train(d, LinearTopology(1, 'metropolis', 1));
            
            % Test
            y = n.test(d);
            
            % Check mean-squared error
            testCase.assertLessThan(mean((y - d.Y).^2), 10^-3);

        end
        
    end  
    
end

