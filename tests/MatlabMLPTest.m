classdef MatlabMLPTest < matlab.unittest.TestCase

    
    methods (Test)

        % Test evaluation of cost function and gradient norm
        function testFvalAndGval(testCase)
           
            % Take seed
            s = rng();
            
            % Define network and dataset
            n = MatlabMLP('', 2, 'trainAlgo', 'trainscg', 'epochs', 1, 'lambda', 0.1);
            dataset = Dataset('', [.1 .2; .3 .4], [1 2; -2 -1], Tasks.R);
            
            % Fake network initialization
            net = feedforwardnet(2);    % Fake initialization for rng
            net.trainFcn = 'trainscg';                   % Set training algorithm
            net.trainParam.showWindow = false;              % Disable output
            net.trainParam.epochs = 1;           % Set maximum number of iterations
            net.trainParam.min_grad = 10^-6;        % Set minimum gradient
            net.layers{1}.transferFcn = 'logsig'; % Set activation function (hidden)
            net.layers{2}.transferFcn = 'logsig'; % Set activation function (output)
            net.inputs{1}.processFcns = {};                 % Disable processing for inputs
            net.outputs{2}.processFcns = {};                % Disable processing for outputs 
            net.performParam.regularization = 0.1;  % Regularization coefficient
            net.divideFcn = '';                             % Disable early stopping
            net = configure(net, dataset.X', dataset.Y');
            net = init(net);
            n = n.init(dataset);
            
            % Make forward and backward pass
            [pre, post] = n.forwardPass(dataset.X);
            [grad_l, grad_w] = n.backwardPass(pre, post, dataset.X, dataset.Y);
            
            % Reset seed
            rng(s);
            
            % Train the network
            [~, trainInfo] = n.train(dataset);
            
            % Compute manually cost function
             fval = (0.9/4)*((post{2}(1,1) - dataset.Y(1,1))^2 + ...
                (post{2}(1,2) - dataset.Y(1,2))^2 + ...
                (post{2}(2,1) - dataset.Y(2,1))^2 + ...
                (post{2}(2,2) - dataset.Y(2,2))^2) + ...
                (0.1/12)*norm([n.W_in_h(:); n.W_h_out(:)])^2;
            
            % Compute manually the gradient
            gval = norm((0.9/4)*[grad_l{1}(:); grad_l{2}(:)] + (0.1/12)*[grad_w{1}(:); grad_w{2}(:)]);
            
            % Check validity
            testCase.assertLessThan(abs(trainInfo.fval(1) - fval), 10^-6);
            testCase.assertLessThan(abs(trainInfo.gval(1) - gval), 10^-6);
            
            
        end
        
        function testXOR(testCase)
            
            % Test using the XOR example taken from:
            % http://matlabgeeks.com/tips-tutorials/neural-networks-a-multilayer-perceptron-in-matlab/
            
            rng(1);
            
            n = MatlabMLP('', 2, 'trainAlgo', 'trainscg', 'epochs', 10000, 'lambda', 10^-8, 'threshold', 10^-6);
            d = Dataset('', [0 0; 0 1; 1 0; 1 1], [0.1; 0.9; 0.9; 0.1], Tasks.R);

            % Initialize the network
            n = n.init(d, 'uniform');
            
            % Train the network
            [n, trainInfo] = n.train(d);
            
            % Test
            y = n.test(d);
            
            % Check mean-squared error
            testCase.assertLessThan(mean((y - d.Y).^2), 10^-6);

        end
            
    end  
    
end

