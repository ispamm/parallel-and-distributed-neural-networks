classdef MultilayerPerceptronTest < matlab.unittest.TestCase

    
    methods (Test)

        function testNetworkCreation(testCase)
            
            n = MultilayerPerceptron('', 2);
            d = Dataset('', [0.5, 0.1 2; 1 2 3], [0.01, 0.99], Tasks.R);
            
            n = n.init(d);
            
            testCase.assertEqual(n.N_inputs, 3);
            testCase.assertEqual(n.N_outputs, 2);
            testCase.assertSize(n.W_in_h, [4, 2]);
            testCase.assertSize(n.W_h_out, [3, 2]);
            
        end
        
        function testForwardPass(testCase)
            
            n = MultilayerPerceptron('', 2);
            d = Dataset('', [0.1 0.2; 0.3 0.4; 0.5 0.6; 0.7 0.8], [-1; 1; 1; -1], Tasks.R);
            
            n = n.init(d);
            
            % Set manually the weights
            W_in_h = rand(3, 2)*2 - 1;
            W_h_out = rand(3, 1)*2 - 1;
            n.W_in_h = W_in_h;
            n.W_h_out = W_h_out;
            
            sigmoid = @(s) 1/(1+exp(-s));
            
            % Forward pass for input 1 --> hidden node 1
            h1 = d.X(1,1)*W_in_h(1,1) + d.X(1,2)*W_in_h(2,1) + W_in_h(3,1);
            x1 = sigmoid(h1);
            
            % Forward pass for input 1 --> hidden node 2
            h2 = d.X(1,1)*W_in_h(1,2) + d.X(1,2)*W_in_h(2,2) + W_in_h(3,2);
            x2 = sigmoid(h2);
            
            % Forward pass hidden nodes --> output node
            o = x1*W_h_out(1,1) + x2*W_h_out(2,1) + W_h_out(3,1);
            y = sigmoid(o);
            
            % Make full forward pass in network
            [pre, post] = n.forwardPass(d.X);
            
            % Check validity of results
            testCase.assertLessThan(abs(pre{1}(1,:) - [h1 h2]), 10^-10);
            testCase.assertLessThan(abs(post{1}(1,:) - [x1 x2]), 10^-10);
            testCase.assertLessThan(abs(pre{2}(1,1) - o), 10^-10);
            testCase.assertLessThan(abs(post{2}(1,1) - y), 10^-10);
            
            
        end
        
        function testBackwardPass(testCase)
            
            n = MultilayerPerceptron('', 2);
            d = Dataset('', [0.1 0.2; 0.3 0.4; 0.5 0.6; 0.7 0.8], [-1; 1; 1; -1], Tasks.R);
            
            n = n.init(d);
            
            % Set manually the weights
            W_in_h = rand(3, 2)*2 - 1;
            W_h_out = rand(3, 1)*2 - 1;
            n.W_in_h = W_in_h;
            n.W_h_out = W_h_out;
            
            % Make full forward pass in network
            [pre, post] = n.forwardPass(d.X(1,:));
            
            % Compute delta value for output neuron
            delta_output = post{2}(1)*(1-post{2}(1))*(d.Y(1) - post{2}(1));
            
            % Propagate delta value to hidden layer
            delta_h1 = post{1}(1,1)*(1-post{1}(1,1))*W_h_out(1)*delta_output;
            delta_h2 = post{1}(1,2)*(1-post{1}(1,2))*W_h_out(2)*delta_output;
            
            % Compute gradients
            grad_hidden1 = -2*[d.X(1,1)*delta_h1; d.X(1,2)*delta_h1; delta_h1];
            grad_hidden2 = -2*[d.X(1,1)*delta_h2; d.X(1,2)*delta_h2; delta_h2];
            grad_output = -2*[delta_output*post{1}(1,1); delta_output*post{1}(1,2); delta_output];
            
            % Make full backward pass in network
            grads = n.backwardPass(pre, post, d.X(1,:), d.Y(1,:));
            
            % Check validity of results
            testCase.assertLessThan(abs(grads{1}(:, 1) - grad_hidden1), 10^-10);
            testCase.assertLessThan(abs(grads{1}(:, 2) - grad_hidden2), 10^-10);
            testCase.assertLessThan(abs(grads{2} - grad_output), 10^-10);
            
        end
        
        function testBackwardPass_Symbolic(testCase)
            
            n = MultilayerPerceptron('', 2);
            d = Dataset('', [0.1 0.2; 0.3 0.4;], [-1; 1;], Tasks.R);
            
            n = n.init(d);
            
            % Set manually the weights
            W_in_h = rand(3, 2)*2 - 1;
            W_h_out = rand(3, 1)*2 - 1;
            n.W_in_h = W_in_h;
            n.W_h_out = W_h_out;
            
            % Define cost function
            sW_in_h = sym('sW_in_h', [3, 2], 'real');
            sW_h_out = sym('sW_h_out', [3, 1], 'real');
            sigmoid = @(s) 1./(1+exp(-s));
            f_net = @(x) sigmoid(sW_h_out'*([sigmoid(sW_in_h'*[x; 1]); 1]));
            squaredError = (f_net([0.1; 0.2]) + 1)^2 + (f_net([0.3; 0.4]) - 1)^2;
            regularizer = norm([sW_in_h(:); sW_h_out(:)])^2;
            
            % Compute symbolic gradients
            sGrads_squaredError = gradient(squaredError, [sW_in_h(:); sW_h_out(:)]);
            sGrads_squaredError = double(subs(sGrads_squaredError, [sW_in_h(:); sW_h_out(:)], [W_in_h(:); W_h_out(:)]));
            sGrads_regularizer = gradient(regularizer, [sW_in_h(:); sW_h_out(:)]);
            sGrads_regularizer = double(subs(sGrads_regularizer, [sW_in_h(:); sW_h_out(:)], [W_in_h(:); W_h_out(:)]));
            
            % Make forward pass in network
            [pre, post] = n.forwardPass(d.X);

            % Make backward pass in network
            [grads_l, grads_r] = n.backwardPass(pre, post, d.X, d.Y);
            grads_l = [grads_l{1}(:); grads_l{2}(:)];
            grads_r = [grads_r{1}(:); grads_r{2}(:)];
            
            % Check validity of results
            testCase.assertLessThan(abs(sGrads_squaredError - grads_l), 10^-10);
            testCase.assertLessThan(abs(sGrads_regularizer - grads_r), 10^-10);
            
        end
  
    end  
    
end

